 LoginManager = (function() {

    function LoginManager(container) {
      var action;
      this.container = container;
      this.hideAll = __bind(this.hideAll, this);

      this.showForgotForm = __bind(this.showForgotForm, this);

      this.showRegisterForm = __bind(this.showRegisterForm, this);

      this.showLoginForm = __bind(this.showLoginForm, this);

      this.loginForm = this.container.find("#login");
      this.registerForm = this.container.find("#register");
      this.forgotForm = this.container.find("#forgot");
      this.loginLink = this.container.find("#login-link");
      this.forgotLink = this.container.find("#forgot-link");
      this.registerLink = this.container.find("#register-link");
      this.loginLink.bind("click", this.showLoginForm);
      this.forgotLink.bind("click", this.showForgotForm);
      this.registerLink.bind("click", this.showRegisterForm);
      this.loginSubmit = this.container.find("#login-submit");
      this.loginSubmit.click(function(e) {
        var wrapper;
        if ($(this).closest("form").find("#email").val().length === 0) {
          e.preventDefault();
          wrapper = $(this).closest(".login-wrapper");
          wrapper.addClass("wobble");
          Notifications.push({
            text: "<i class='icon-warning-sign'></i> invalid username or password",
            autoDismiss: 3,
            "class": "error"
          });
          return wrapper.bind("webkitAnimationEnd animationEnd mozAnimationEnd", function() {
            wrapper.off("webkitAnimationEnd");
            return wrapper.removeClass("wobble");
          });
        }
      });
      action = this.getParameterByName("action");
      if (action === 'register') {
        this.showRegisterForm();
      } else if (action === 'forgot-password') {
        this.showForgotForm();
      } else {
        this.showLoginForm();
      }
    }

    LoginManager.prototype.showLoginForm = function() {
      this.hideAll();
      this.loginForm.show();
      this.registerLink.show();
      return this.forgotLink.show();
    };

    LoginManager.prototype.showRegisterForm = function() {
      this.hideAll();
      this.registerForm.show();
      this.loginLink.show();
      return this.forgotLink.show();
    };

    LoginManager.prototype.showForgotForm = function() {
      this.hideAll();
      this.forgotForm.show();
      this.loginLink.show();
      return this.registerLink.show();
    };

    LoginManager.prototype.hideAll = function() {
      this.loginForm.hide();
      this.registerForm.hide();
      this.forgotForm.hide();
      this.loginLink.hide();
      this.forgotLink.hide();
      return this.registerLink.hide();
    };

    LoginManager.prototype.getParameterByName = function(name) {
      var regex, regexS, results;
      name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
      regexS = "[\\?&]" + name + "=([^&#]*)";
      regex = new RegExp(regexS);
      results = regex.exec(window.location.search);
      if (results === null) {
        return "";
      } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
      }
    };

    return LoginManager;

  })();

  $(function() {
    return new LoginManager($("#login-manager"));
  });

}).call(this);
(function() {

  $(function() {
    return $(".search-button-trigger").click(function() {
      return $(".search-bar-nav").toggleClass("open");
    });
  });

}).call(this);



(function() {

  $(function() {
    var _this = this;
    if ($("body").hasClass("login")) {
      Notifications.push({
        imagePath: "../../images/cloud.png",
        text: "<p>Welcome to the Plastique theme!</p><div>Click login to get a WOW wrong username/password effect, or write any username to enter</div> <div>Be sure to check all sections for features!</div>",
        autoDismiss: 10
      });
    } else {
      new Notifications({
        container: $("body"),
        bootstrapPositionClass: "span8 offset2"
      });
    }
    $("#notification-full-image").click(function() {
      return Notifications.push({
        imagePath: "../../images/boy_avatar.png",
        fillImage: true,
        text: "<p><b>Hello there</b></p> This is a sample notification that has an image that fills up the left side",
        time: "just now"
      });
    });
    $("#notification-small-image").click(function() {
      return Notifications.push({
        imagePath: "../../images/cloud.png",
        text: "This is a sample notification that has a small image that sits centered on the left",
        time: "a few seconds ago"
      });
    });
    $("#notification-callback").click(function() {
      return Notifications.push({
        imagePath: "../../images/cloud.png",
        text: "<p><b>Hello there</b></p> This notification is registered with a callback when dismissed!",
        dismiss: function() {
          return Notifications.push({
            imagePath: "../../images/yodawg.png",
            fillImage: true,
            text: "<p>Yo dawg, I heard you like notifications, so when you dismissed that notification we registered another notification in your notification so you can read your new notification after you dismiss the first notification</p>",
            time: "a few seconds ago"
          });
        }
      });
    });
    return $("#notification-auto-dismiss").click(function() {
      return Notifications.push({
        imagePath: "../../images/cloud.png",
        text: "<p><b>Good day sire</b> This notification will expire in 5 seconds</p>",
        autoDismiss: 5
      });
    });
  });

}).call(this);