<?php 

use Offer\OfferFactory; 

class OfferController extends BaseController
{

	public function index($type = null)
	{

		if($type)
		{
		$offer = OfferFactory::create($type); 
		$model = $offer->getModel(); 


		

		return View::Make($offer->getViewTemplate())->with(array('adapter' => $offer,  'offers' => $model::all())); 
		}
		else
		{

			$adapter = new \Offer\Adapter\CoreAdapter(); 
			$adapter->setTranslator(new \Offer\Translator\TranslatorAbstract());  

			return View::Make('offer.list.all')->with(array('adapter' => $adapter, 'offers' => Offer::all())); 
		}

		 
	}




}