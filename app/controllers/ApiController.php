<?php
class ApiController extends BaseController
{
    
    public function config() {
        
          $userId = Sentry::getUser()->id;
          return View::Make('api.config')->with('api' , Api::where('user_id' , '=', $userId)->first()); 
    }
    
    public function saveconfig() {
        
        $type = Input::get('type');
        
        switch ($type) {
            case 1:
            case 4:
                $rules = array();
                $messages = array();
                break;

            case 2:
               
                $rules = array(
                    'otodom_url' => array(
                        'required',
                        'regex:/.*?-id[0-9]*/'),
                        'type' => 'required|numeric'
                    );
                    $messages = array(
                        'required' => 'Te pole jest wymagane',
                        'regex' => 'Format wpisu jest niepoprawny',
                        'numeric' => 'Niepoprawy format wpisu'
                    );
                    break;

                case 3:
                    $rules = array(
                        'site_url' => 'required|active_url',
                        'template_type' => 'required',
                        'type' => 'required|numeric'
                    );
                    
                    $messages = array(
                        'required' => 'Te pole jest wymagane',
                        'regex' => 'Format wpisu jest niepoprawny',
                        'active_url' => 'Podany adres nie odpowiada'
                    );
                    break;
            }
            
           $validator = Validator::make(Input::all() , $rules, $messages);
            
            if ($validator->passes()) {
                
                $model = Api::firstOrNew(array('user_id' => Sentry::getUser()->id));                
                $model->fill(Input::all()); 
                $model->save();
            } else {
                return Redirect::route('api_config')->withErrors($validator)->withInput();
            }
            
            return View::Make('api.config')->with('api' , $model);
        }
    }