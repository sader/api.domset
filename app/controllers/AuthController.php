<?php
class AuthController extends BaseController
{
    
    /**
     * Display the login page
     * @return View
     */
    public function getLogin() {

        if(Sentry::check())
        {
            return Redirect::route('wall'); 
        }


        return View::make('auth.login');
    }
    
    /**
     * Login action
     * @return Redirect
*/
    public function postLogin() {
        $credentials = array('email' => Input::get('email'), 'password' => Input::get('password'));
        
        try {
            $user = Sentry::authenticateAndRemember($credentials);
            
            if ($user) {
                return Redirect::route('wall');
            }
        }
        catch(\Exception $e) {

            die(var_dump($e->getMessage()));  

            return Redirect::route('login')->withErrors(array('login' => $e->getMessage()));
        }
    }
    
    /**
     * Logout action
     * @return Redirect
     */
    public function getLogout() {
        Sentry::logout();
        
        return Redirect::route('login');
    }
}
