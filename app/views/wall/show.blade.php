@extends('layouts.master')
 



@section('content')

<body>
<section id="main">
  <div class="top-nav">
  <div class="container-fluid">
    <div class="row-fluid search-button-bar-container">
      <div class="span12">
        <ul class="breadcrumb">
          <li><a href="#"><i class="icon-home"></i> Some</a></li>
          <li><a href="#">Nice</a></li>
          <li><a href="#">Breadcrumbs</a></li>
          <li class="active"><a href="#">Here</a></li>
        </ul>
        <a class="search-button-trigger" href="#"><i class="icon-search"></i></a>
      </div>
    </div>

    <div class="row-fluid search-bar-nav">
      <div class="span12">
        <form>
          <input type="text" class="search" placeholder="Search...">
        </form>
      </div>
    </div>
  </div>
</div>

  <div class="container-fluid">
    <div class="row-fluid">
  <div class="span6">
    <div class="tabbable black-box" style="margin-bottom: 18px;">

  <div class="tab-header">
    Some Title here
        <span class="pull-right">
          <span class="options">
            <div class="btn-group">
              <a class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></a>
              <ul class="dropdown-menu black-box-dropdown dropdown-left">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
          </span>
        </span>
  </div>

  <ul class="nav nav-tabs">
    <li class=""><a href="#tab1" data-toggle="tab">Section 1</a></li>
    <li class=""><a href="#tab2" data-toggle="tab">Section 2</a></li>
    <li class="active"><a href="#tab3" data-toggle="tab">Section 3</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane" id="tab1">
      <div class="padded">
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color blue</span>
              <span class="badge blue">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color gray</span>
              <span class="badge gray">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color red</span>
              <span class="badge red">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color green</span>
              <span class="badge green">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color </span>
              <span class="badge ">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color black</span>
              <span class="badge black">1</span>
            </p>
        
      </div>
    </div>
    <div class="tab-pane" id="tab2">
      <div class="padded">
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color gray</span>
              <span class="badge gray">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color green</span>
              <span class="badge green">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color blue</span>
              <span class="badge blue">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color black</span>
              <span class="badge black">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color red</span>
              <span class="badge red">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color </span>
              <span class="badge ">1</span>
            </p>
        
      </div>
    </div>
    <div class="tab-pane active" id="tab3">
      <div class="separator">
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color </span>
              <span class="badge ">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color blue</span>
              <span class="badge blue">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color gray</span>
              <span class="badge gray">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color green</span>
              <span class="badge green">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color black</span>
              <span class="badge black">1</span>
            </p>
        
            <p>
              <span style="display: inline-block; min-width: 150px;">badge color red</span>
              <span class="badge red">1</span>
            </p>
        
      </div>

      <div class="padded">
        <div class="inner-well clearfix">
          <b>Some option here</b>
          <div class="pull-right">
            <input type="checkbox" id="checky2" class="checky"/>
            <label for="checky2" class="checky"><span></span></label>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>

  <div class="span6">
    <div class="nav-menu box">
  <ul class="nav nav-list">
    <li class="active">
      <a href="#">
        <i class="icon-home"></i> Home
        <span class="pull-right badge blue">1</span>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="icon-book"></i> Library
        <span class="pull-right badge">1</span>
      </a>
    </li>
    <li><a href="#"><i class="icon-folder-close"></i> Stuff</a></li>
    <li class="nav-header">List header</li>
    <li><a href="#"><i class="icon-home"></i> Home</a></li>
    <li><a href="#"><i class="icon-book"></i> Library</a></li>
    <li><a href="#"><i class="icon-folder-close"></i> Stuff</a></li>
    <li class="nav-header">Another list header</li>
    <li><a href="#"><i class="icon-user"></i> Profile</a></li>
    <li><a href="#"><i class="icon-wrench"></i> Settings</a></li>
    <li><a href="#"><i class="icon-question-sign"></i> Help</a></li>
  </ul>
</div>
  </div>
</div>

<div class="row-fluid">
  <div class="span6">
    <div class="black-box tex">
  <div class="tab-header">
    Recent comments
  </div>
  <ul class="recent-comments">

    @foreach($posts as $post) 

        <li class="separator">
          <div class="avatar pull-left">
            <img src="{{ Croppa::url ('/images/wall/'.$post->image, 48, 48) }}"/>
          </div>

          <div class="article-post">
            <div class="user-info"> Posted by jordan, 3 days ago </div>
            <div class="user-content">
                {{{ $post->description }}}
            </div>

            <div class="btn-group">
              <button class="button black mini"><i class="icon-pencil"></i> Edit</button>
              <button class="button black mini"><i class="icon-remove"></i> Delete</button>
              <button class="button black mini"><i class="icon-ok"></i> Approve</button>
            </div>
          </div>
        </li>
        @endforeach; 
    

        <li class="separator">
          <div class="avatar pull-left">
            <img src="../../images/avatar.png" />
          </div>

          <div class="article-post">
            <div class="user-info"> Posted by jordan, 3 days ago </div>
            <div class="user-content">
              Vivamus sed auctor nibh congue, ligula vitae tempus pharetra...
              Vivamus sed auctor nibh congue, ligula vitae tempus pharetra...
              Vivamus sed auctor nibh congue, ligula vitae tempus pharetra...
            </div>

            <div class="btn-group">
              <button class="button black mini"><i class="icon-pencil"></i> Edit</button>
              <button class="button black mini"><i class="icon-remove"></i> Delete</button>
              <button class="button black mini"><i class="icon-ok"></i> Approve</button>
            </div>
          </div>
        </li>

    
    <li class="separator" style="text-align: center">
      <a href="#">View all</a>
    </li>
  </ul>
</div>
  </div>

  <div class="span6">
    <div class="box" style="position:relative;">
<div class="tab-header" >
  Browser stats
</div>

</div>
  </div>
</div>
    <div class="row-fluid">
  <div class="span12">
    <div class="footer">
      <div>2012 &copy; Plastique Admin Theme</div>
      <div>Carefully crafted by <a href="https://wrapbootstrap.com/user/andrei4002">andrei4002</a></div>
    </div>
  </div>
</div>
  </div>
</section>


@stop


@section('navigate')

@parent

@stop
