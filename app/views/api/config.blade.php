@extends('layouts.master')
@section('content')
<body>
    <section id="main">
        <div class="top-nav">
            <div class="container-fluid">
                <div class="row-fluid search-button-bar-container">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="icon-home"></i> Some</a></li>
                            <li><a href="#">Nice</a></li>
                            <li><a href="#">Breadcrumbs</a></li>
                            <li class="active"><a href="#">Here</a></li>
                        </ul>
                        <a class="search-button-trigger" href="#"><i class="icon-search"></i></a>
                    </div>
                </div>
                <div class="row-fluid search-bar-nav">
                    <div class="span12">
                        <form>
                            <input type="text" class="search" placeholder="Search...">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
            
                <div class="span12">
   <div class="panel-group" id="accordion">
{{ Form::model($api) }}
   <div class="span12">
    <div class="tabbable black-box" style="margin-bottom: 18px;">

  <div class="tab-header">
    Ustawienie synchronizacji ofert
  </div>
 
  <ul id="setting-tab" class="nav nav-tabs">
    <li><a href="#tab1" data-id="1" data-toggle="tab">Brak synchronizacji</a></li>
    <li><a href="#tab2" data-id="2" data-toggle="tab">Synchronizacja z Otodom.pl</a></li>
    <li><a href="#tab3" data-id="3" data-toggle="tab">Synchronizacja ze stroną</a></li>
    <li><a href="#tab4" data-id="4" data-toggle="tab">DomsetAPI</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab1">
      <div class="padded">
           <h2> Brak synchronizacji </h2>
                    
                    Jeśli masz problem z wyborem odpowiedniej opcji, skontaktuj się z administratorem

      </div>
    </div>
    
    <div class="tab-pane" id="tab2">

      <div class="padded">

       
         <h2> Pobierz oferty bezpośrednio z serwisu otodom.pl </h2>
    <div class="note">Podaj identyfikator w postaci nazwa_biura_idXXXXX np. domset-marcin-brochacki-id239317</div>
        <div>{{ Form::text('otodom_url' , null, array('class' => 'input-transparent fill-up')) }}</div>
        {{ $errors->first('otodom_url', '<p class="error">:message</p>'); }}
       
    </div>
    </div>
    <div class="tab-pane" id="tab3">
      <div class="padded">

        <h2>Pobierz oferty bezpośrednio z mojej strony</h2>
                   <div class="note">Podaj adres strony :</div>
                   <div>{{ Form::text('site_url' , null, array('class' => 'input-transparent fill-up')) }}</div>
                   {{ $errors->first('site_url', '<p class="error">:message</p>'); }}

                   <div class="note">Wybierz szablon </div>
                   {{ Form::select('template_type' , 
                                    array(1 => 1 , 2 => 2 , 3 => 3), 
                                    null, 
                                    array('class' => 'input-transparent')) }}
                 <p><a href="#" id="test-page-connect"  class="btn btn-success">Testuj zgodność</a></p>
                   </div>
    </div>
    <div class="tab-pane" id="tab4">
      <div class="padded">
          
       <h2>Importy z wykorzystaniem DomsetAPI. </h2>
                


                <p>Twój klucz dostępu do domsetAPI 235946798ry79sd7gd9f7g9d67g9df6g789</p>
                <p>Zapoznaj się z <a href="/documentation.pdf">dokumentacją DomsetAPI</a></p>

      </div>
    </div>
    {{ Form::hidden('type', null, array('id' => 'type'))}}
    <p style="text-align: center">
    <input type="submit" class="btn btn-inverse btn-large" id="submit"  value="Ustaw aktualny wybór" /></p>

  </div>
</div>  
{{ Form::close() }}
  </div></div>
        
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</section>

<script>

$(document).ready(function()
{

  if($('#type').val() !== '')
  {

    $('a[data-id=' + $('#type').val() + ']').click(); 
   //  $('a[data-id=' + $('#type').val() + ']').parent().addClass('active'); 
   //  $('#tab' +  $('#type').val()).addClass('active'); 
  }

 $('#setting-tab li  a').click(function()
 {
    $('#type').val($(this).attr('data-id')); 
  }); 

}); 

</script>

@stop

               