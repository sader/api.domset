doctype html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>L4 Site</title>
         {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/bootstrap-responsive.css') }}
      <?php //  @include('admin._partials.assets') ?>
</head>
<body>
<div class="container">
        <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
                <div class="container">
                        
                        
                        
                              <?php //  @include('@include('admin._partials.navigation')') ?>
                        
                </div>
        </div>
</div>
 
<hr>
 
        @yield('main')
</div>
    
         {{ HTML::script('js/jquery.v1.8.3.min.js') }}
        {{ HTML::script('js/bootstrap/bootstrap.min.js') }}
</body>
</html>