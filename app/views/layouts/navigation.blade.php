
@section('navigate')
<nav id="primary" class="main-nav">
  <ul>

    <li class="{{ Route::is( 'wall') ? 'active' : '' }}">
      <a href="{{ URL::route( 'wall') }}">
        <i class="icon-dashboard"></i> Ściana
      </a>
    </li>
    <li class="{{ Route::is( 'offer') ? 'active' : '' }}">
      <a href="{{ URL::route( 'offer') }}">
        <i class="icon-dashboard"></i> Oferty
      </a>
    </li>
    <li>
      <a href="#">
          <i class="icon-beaker"></i> Konto
      </a>
    </li>

    <li class="{{ Route::is( 'api_config') ? 'active' : '' }}">>
      <a href="{{ URL::route( 'api_config') }}">
          <i class="icon-plus-sign"></i> API
      </a>
    </li>

  </ul>
</nav>
@show