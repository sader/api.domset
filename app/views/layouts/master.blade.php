<html>
    <head>
      <title>@yield('title' , 'Domset.pl')</title>
        {{ HTML::style('css/application.css') }}
        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::script('/js/jquery-1.11.0.min.js') }}
        {{ HTML::script('/js/bootstrap.min.js') }}
        {{ HTML::script('/js/jquery.dataTables.min.js') }}
        {{ HTML::script('/js/jquery.dataTables.filters.min.js') }}
    </head>
        <body>

        @include('layouts.navigation')
        @yield('content')

        </div>
      
     @if($errors->has())
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
     @endforeach
    @endif
        
     
    </body>

</html>