@extends('layouts.master')
@section('content')
<body>
    <section id="main">
        <div class="top-nav">
            <div class="container-fluid">
                <div class="row-fluid search-button-bar-container">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="icon-home"></i> Some</a></li>
                            <li><a href="#">Nice</a></li>
                            <li><a href="#">Breadcrumbs</a></li>
                            <li class="active"><a href="#">Here</a></li>
                        </ul>
                        <a class="search-button-trigger" href="#"><i class="icon-search"></i></a>
                    </div>
                </div>
                <div class="row-fluid search-bar-nav">
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                  

                    <div class="box">
                                <ul id="filtered">
                                    <li>Type : <p id="typeidselector"></p></li>
                                    <li>Właściciel : <p id="useridselector"></p></li>
                                    <li>Województwo<p id="provinceselector"></p></li>
                                    <li>Powiat : <p id="communityselector"></p></li>
                                    <li>Gmina : <p id="distrinctselector"></p></li>
                                    <li>Miasto : <p id="cityselector"></p></li>
                                    <li>Dzielnica : <p id="sectionselector"></p></li>
                                    <li>Ulica : <p id="streetselector"></p></li>
                                    <li>Powierzchnia : <p id="surfaceselector"></p></li>
                                    <li>Cena : <p id="priceselector"></p></li>
                                    <li>Cena negocjowalna : <p id="pricenegotiableselector"></p></li>
                                    <li> Cena z 1m<sup>2</sup><p id="pricesurfaceselector"></p></li>
                                </ul>

                        
                        <table class="table table-striped dataTable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Typ ofery</th>
                                    <th>Właściciel</th>
                                    <th>Województwo</th>
                                    <th>Powiat</th>
                                    <th>Gmina</th>
                                    <th>Miasto</th>
                                    <th>Dzielnica</th>
                                    <th>Ulica</th>
                                    <th>Powierzchnia</th>
                                    <th>Cena</th>
                                    <th>Cena do negocjacji</th>
                                    <th>Cena za 1m<sup>2</sup></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($offers as $offer)    
                                <tr class="gradeX">
                                    <td><img src="{{ Croppa::url('/images/gallery/'.$offer->id.'/1.jpg', 150 , 100) }}" /></td>
                                    <td>{{ $adapter->getNameById($offer->type_id) }}</td>
                                    <td>{{ $offer->user->email }}</td>
                                    <td>{{ $adapter->translate('province' ,  $offer->province) }}</td>
                                    <td>{{ $offer->community }}</td>
                                    <td>{{ $offer->distrinct }}</td>
                                    <td>{{ $offer->city }}</td>
                                    <td>{{ $offer->section }}</td>
                                    <td>{{ $offer->street }}</td>
                                    <td>{{ $offer->surface }}</td>
                                    <td>{{ $offer->price }}</td>
                                    <td>{{ $offer->price_negotiable  ? 'Tak' : 'Nie' }}</td>
                                    <td>{{ $offer->price_surface }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                     <th></th>

                            </tfoot>
                        </table>
                           <script>
        $('.dataTable').dataTable({
                         "bJQueryUI": true,
                            "sPaginationType": "full_numbers",
                            "sDom": '<""l>t<"F"fp>' }).columnFilter({
                            aoColumns: [
                                    null,
                                    { sSelector: "#typeidselector", type: "select"},
                                    { sSelector: "#useridselector", type: "select"},
                                    { sSelector: "#provinceselector", type: "select"},
                                    { sSelector: "#communityselector", type: "text"},
                                    { sSelector: "#distrinctselector", type: "text"},
                                    { sSelector: "#cityselector", type: "text"},
                                    { sSelector: "#sectionselector", type: "text"},
                                    { sSelector: "#streetselector", type: "text"},
                                    { sSelector: "#surfaceselector", type: "number-range"},
                                    { sSelector: "#priceselector", type: "number-range"},
                                    { sSelector: "#pricenegotiableselector", type: "select" , values: ['Tak' , 'Nie']},
                                    { sSelector: "#pricesurfaceselector", type: "number-range"}]

        });   

        </script>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="footer">
            <div>2012 &copy; Plastique Admin Theme</div>
            <div>Carefully crafted by <a href="https://wrapbootstrap.com/user/andrei4002">andrei4002</a></div>
        </div>
    </div>
</div>
</div>
</section>
@stop


@section('navigate')
@parent
<nav id="secondary" class="main-nav">
<ul class="secondary-nav-menu">
<li>
    <a href="{{ URL::route('offer') }}">
    <i class="icon-plus-sign"></i> Wszystkie
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 1)) }}">
    <i class="icon-plus-sign"></i> Mieszkania sprzedaż
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 2)) }}">
    <i class="icon-plus-sign"></i> Mieszkania wynajem
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 3)) }}">
    <i class="icon-plus-sign"></i> Domy sprzedaż
    </a>
</li>
<li>
      <a href="{{ URL::route('offer' , array('type' => 4)) }}">
    <i class="icon-plus-sign"></i> Domy kupno
    </a>
</li>
<li>
      <a href="{{ URL::route('offer' , array('type' => 5)) }}">
    <i class="icon-plus-sign"></i> Lokale kupno
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 6)) }}">
    <i class="icon-plus-sign"></i> Lokale kupno
    </a>
</li>
<li>
     <a href="{{ URL::route('offer' , array('type' => 7)) }}">
    <i class="icon-plus-sign"></i> Działki kupno
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 8)) }}">
    <i class="icon-plus-sign"></i> Działki kupno
    </a>
</li>
<li>
   <a href="{{ URL::route('offer' , array('type' => 9)) }}">
    <i class="icon-plus-sign"></i> Magazyny kupno
    </a>
</li>
<li>
     <a href="{{ URL::route('offer' , array('type' => 10)) }}">
    <i class="icon-plus-sign"></i> Magazyny kupno
    </a>
</li>
</ul>
</nav>
@stop