@extends('layouts.master')
@section('content')
<body>
    <section id="main">
        <div class="top-nav">
            <div class="container-fluid">
                <div class="row-fluid search-button-bar-container">
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li><a href="#"><i class="icon-home"></i> Some</a></li>
                            <li><a href="#">Nice</a></li>
                            <li><a href="#">Breadcrumbs</a></li>
                            <li class="active"><a href="#">Here</a></li>
                        </ul>
                        <a class="search-button-trigger" href="#"><i class="icon-search"></i></a>
                    </div>
                </div>
                <div class="row-fluid search-bar-nav">
                    <div class="span12">
                        <form>
                            <input type="text" class="search" placeholder="Search...">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="box">
                        <table class="table table-striped dataTable">
                            <thead>
                                <tr>
                                    <th>type_id</th>
                                    <th>user_id</th>
                                    <th>province</th>
                                    <th>community</th>
                                    <th>distrinct</th>
                                    <th>city</th>
                                    <th>section</th>
                                    <th>street</th>
                                    <th>surface</th>
                                    <th>price</th>
                                    <th>price_negotiable</th>
                                    <th>price_surface</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($offers as $offer)    
                                <tr class="gradeX">

                                    <td>{{ $adapter->getNameById($offer->detail->type_id) }}</td>
                                             

                                    <td>{{ $offer->detail->user->email }}</td>
                                    <td>{{ $offer->detail->province }}</td>
                                    <td>{{ $offer->detail->community }}</td>
                                    <td>{{ $offer->detail->distrinct }}</td>
                                    <td>{{ $offer->detail->city }}</td>
                                    <td>{{ $offer->detail->section }}</td>
                                    <td>{{ $offer->detail->street }}</td>
                                    <td>{{ $offer->detail->surface }}</td>
                                    <td>{{ $offer->detail->price }}</td>
                                    <td>{{ $offer->detail->price_negotiable }}</td>
                                    <td>{{ $offer->detail->price_surface }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="footer">
            <div>2012 &copy; Plastique Admin Theme</div>
            <div>Carefully crafted by <a href="https://wrapbootstrap.com/user/andrei4002">andrei4002</a></div>
        </div>
    </div>
</div>
</div>
</section>
@stop
@section('navigate')
@parent
<nav id="secondary" class="main-nav">
<ul class="secondary-nav-menu">
<li>
    <a href="{{ URL::route('offer') }}">
    <i class="icon-plus-sign"></i> Wszystkie
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 1)) }}">
    <i class="icon-plus-sign"></i> Mieszkania sprzedaż
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 2)) }}">
    <i class="icon-plus-sign"></i> Mieszkania wynajem
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 3)) }}">
    <i class="icon-plus-sign"></i> Domy sprzedaż
    </a>
</li>
<li>
      <a href="{{ URL::route('offer' , array('type' => 4)) }}">
    <i class="icon-plus-sign"></i> Domy kupno
    </a>
</li>
<li>
      <a href="{{ URL::route('offer' , array('type' => 5)) }}">
    <i class="icon-plus-sign"></i> Lokale kupno
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 6)) }}">
    <i class="icon-plus-sign"></i> Lokale kupno
    </a>
</li>
<li>
     <a href="{{ URL::route('offer' , array('type' => 7)) }}">
    <i class="icon-plus-sign"></i> Działki kupno
    </a>
</li>
<li>
    <a href="{{ URL::route('offer' , array('type' => 8)) }}">
    <i class="icon-plus-sign"></i> Działki kupno
    </a>
</li>
<li>
   <a href="{{ URL::route('offer' , array('type' => 9)) }}">
    <i class="icon-plus-sign"></i> Magazyny kupno
    </a>
</li>
<li>
     <a href="{{ URL::route('offer' , array('type' => 10)) }}">
    <i class="icon-plus-sign"></i> Magazyny kupno
    </a>
</li>
</ul>
</nav>
@stop