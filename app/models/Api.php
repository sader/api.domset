<?php

class Api extends Eloquent {

protected $table = 'api'; 
protected $fillable = array('user_id' , 'type' , 'otodom_url' , 'site_url' , 'template_type');

	

	public function scopeNeedToDoJob($query)
	{
		return $query->whereIn('type', array(\Crawler\CrawlerAbstract::TYPE_OTODOM, 
											 \Crawler\CrawlerAbstract::TYPE_PARTNER_SITE)); 
	}

	public function scopeOldEnough($query, $hours = 3)
	{

		return $query->where("updated_at", "<", DB::raw('NOW() - INTERVAL ' . $hours . ' HOUR')); 
	}

	public function oldest($query)
	{
		return $query->orderBy('updated_at', 'ASC'); 
	}
}

