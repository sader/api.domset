<?php
namespace Crawler\Galactica;

use Crawler\CrawlerAbstract;

class GalacticaAdapter extends CrawlerAbstract
{
    
    private $curl;
    private $translator;
    private $listUrl;
    private $detailUrl;
    private $limitPerPage = 10;
    private $detailDom;
    private $provider = '';
    
    public function __construct($translator, \Curl $curl) {
        $this->curl = $curl;
        $this->curl->option('BINARYTRANSFER', 1)->option('HEADER', 0);
        
        $this->translator = $translator;
    }
    
    public function getProvider() {
        return $this->provider;
    }
    
    public function setProvider($provider) {
        $this->provider = $provider;
        return $this;
    }
    
    public function getListUrl($page = 1) {
        
        return $this->getProvider() . '/Oferty?s=' . $page;
    }
    
    public function setDetailUrl($url) {
        $this->detailUrl = $url;
    }
    
    public function getDetailUrl() {
        return $this->detailUrl;
    }
    
    public function getPageNumber() {
        $offers_num = pq('#ilOfert')->text();
        return ceil($offers_num / $this->limitPerPage);
    }
    
    public function getOffersList() {
        
        $aLinks = array();
        $data = $this->curl->simple_get($this->getListUrl());
        $oDom = $this->getDom($data);
        
        $pages = $this->getPageNumber();
        
        $links = pq('.foto');
        
        foreach ($links as $link) {
            $aLinks[] = pq($link)->parent()->attr('href');
        }
        
        if ($pages > 1) {
            for ($i = 2; $i <= $pages; $i++) {
                
                $data = $this->curl->simple_get($this->getListUrl($i));
                $oDom = $this->getDom($data);
                $links = pq('.foto');
                foreach ($links as $link) {
                    $aLinks[] = pq($link)->parent()->attr('href');
                }
            }
        }
        
        return $this->tested($aLinks);
    }
    
    public function getOfferDetails($link) {
        
        $this->setDetailUrl($link);
        $data = $this->curl->simple_get($this->getDetailUrl());
        $this->detailDom = $this->getDom($data);
        
        $translator = new  Crawler\Galactica\Translator\Core; 
        $sTypeName = $this->detectType($link, $translaor->types);

        $adapterName = "\Crawler\Galactica\OfferType\\" . $sTypeName;
        $translatorName = "\Crawler\Galactica\Translator\\" . $sTypeName;
        
        $adapter = new $adapterName($this->detailDom, new $translatorName);
        $return['data'] = $adapter->getData();
        $return['adapter'] = $sTypeName;
        
        return $return;
    }
    
    public function detectType($link, $types) {
        
        foreach($types as  $type => $adapter)
        {
            if(strpos($type, $link) !== false)
            {
                return $adapter; 
            }
        }

        throw new \Exception('No adapter found'); 

    }
    
    public function getImages() {
        
        $aImages = array();
        $aImagesSrc = json_decode(pq($this->detailDom)->find('#offerGallery')->attr('data-photos'));
        
        if (!empty($aImagesSrc)) {
            foreach ($aImagesSrc as $image) {
                $aImages[] = str_replace('93x70', '525x394', $image);
            }
        }
        
        return $aImages;
    }
    
    public function testUrl($sProviderUrl) {
    }
}

