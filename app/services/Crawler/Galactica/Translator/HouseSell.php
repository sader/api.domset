<?php
namespace \Crawler\Galactica\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class HouseSell extends Core
{
    
    public $building_type = array(
        'wolnostojący' => 1,
        'bliźniak' => 4,
        'szeregowiec' => 2,
        'kamienica czynszowa' => 5,
        'dworek/pałac' => 6,
        'gospodarstwo ' => 8
    );
    
    public $building_material = array(
        'cegła' => 1,
        'drewno' => 4,
        'pustak' => 2,
        'keramzyt' => 7,
        'wielka płyta' => 7,
        'beton' => 7,
        'inne' => 7,
        'silikat' => 5,
        'beton komórkowy' => 7
    );
    
    public $quality = array(
        'do zamieszkania' => 3,
        'do wykończenia' => 7,
        'do remontu' => 6,
        'stan surowy otwarty' => 7,
        'stan surowy zamknięty' => 3
    );
    
   
    
    public $garret = array(
        'brak' => 3,
        'użytkowe' => 1,
        'nieużytkowe' => 2
    );
    
    public $windows = array(
        'plastikowe' => 3,
        'drewniane' => 4,
        'aluminiowe' => 7,
    );
    
    public $media = array(
        'prad' => 1,
        'woda' => 1,
        'gaz' => 1,
        'telefon' => 1,
        'kanalizacja' => 1,
        'szambo' => 1,
        'oczyszczalnia' => 1
    );
    
    public $heat = array(
        'gazowe' => 1,
        'weglowe' => 2,
        'biomasa' => 3,
        'pompa ciepla' => 4,
        'kolektor słoneczny' => 5,
        'geotermika' => 12,
        'olejowe' => 6,
        'elektryczne' => 8,
        'miejskie' => 9,
        'kominkowe' => 10,
        'piece kaflowe' => 11
    );
    
    public $fence = array(
        'murowane' => 3,
        'siatka' => 2,
        'metalowe' => 7,
        'drewniane' => 4,
        'betonowe' => 5,
        'żywopłot' => 6,
        'inne' => 7
    );
    
    public $additional = array(
        'piwnica' => 1,
        'strych' => 1,
        'garaż' => 1,
        'basen' => 1,
        'Internet' => 1,
        'telewizja kablowa' => 1,
        'klimatyzacja' => 1,
        'umeblowanie' => 1
    );

     public $rooms = array(
        '1-pokojowy' => 1,
        '2-pokojowy' => 2,
        '3-pokojowy' => 3,
        '4-pokojowy' => 4,
        '5-pokojowy' => 5,
        '6-pokojowy' => 6,
        '7-pokojowy' => 7
    );

}