<?php namespace \Crawler\Galactica\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LandSell extends Core
{
    
    public $type = array(
        'budowlana' => 1,
        'rolna' => 2,
        'rekreacyjna' => 6,
        'pod inwestycję' => 5,
        'leśna' => 7,
        'siedliskowa ' => 9,
        'inna' => 11,
        'rolno-budowlana' => 10
    );
    
    public $media = array(
        'prad' => 1,
        'woda' => 1,
        'gaz' => 1,
        'telefon' => 1,
        'kanalizacja' => 1,
        'szambo' => 1,
        'oczyszczalnia' => 1
    );
    
    public $access = array(
        'utwardzony' => 1,
        'polny' => 1,
        'asfaltowy' => 1
    );
    
    
}
