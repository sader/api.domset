<?php namespace \Crawler\Galactica\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class WarehouseSell extends Core
{
    
    public $access = array(
        'utwardzony betonowy' => 1,
        'nieutwardzony' => 1,
        'asfalt' => 1
    );
    
    public $media = array(
        'woda' => 1,
        'prąd' => 1,
        'siła' => 1,
        'kanalizacja' => 1,
        'telefon' => 1,
        'gaz' => 1,
        'internet' => 1,
        'szambo' => 1,
        'oczyszczalnia' => 1
    );
    
    public $purpose = array(
        'magazynowe' => 1,
        'produkcyjne' => 1,
        'biurowe' => 1,
        'handlowe' => 1
    );
    
    public $access = array(
        'brak' => 1,
        'utwardzony' => 1,
        'betonowy' => 1,
        'asfaltowy' => 1,
        'nieutwardzony' => 1,
        'kostka brukowa' => 1
    );
    
    public $floor = array(
        'brak' => 1,
        'pylna' => 1,
        'niepylna' => 1
    );
    
    public $quality = array(
        'gotowe' => 1,
        'do wykończenia' => 1,
        'do remontu' => 1,
        'stan surowy otwarty' => 1,
        'stan surowy zamknięty' => 1
    );
    
    public $fence = array(
        'tak' => 1,
        'nieogrodzony' => 1
    );

    public  $heat  = array(
        'tak' => 1,
        'nie' => 0

        ); 
}
