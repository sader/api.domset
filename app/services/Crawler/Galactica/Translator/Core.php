<?php namespace Crawler\Galactica\Translator;

use Offer\Translator\TranslatorAbstract;


class Core extends TranslatorAbstract
{
    
    public $type = array(
        'Mieszkania/Sprzedaz' => 'FlatSell',
        'Mieszkania/Wynajem' => 'FlatRent',
        'Domy/Sprzedaz' => 'HouseSell',
        'Domy/Wynajem' => 'HouseRent',
        'Dzialki/Sprzedaz' => 'LandSell',
        'Dzialki/Wynajem' => 'LandRent',
        'Lokale/Sprzedaz' => 'LocalSell',
        'Lokale/Wynajem' => 'LocalRent',
        'Hale/Sprzedaz' => 'WarehouseSell',
        'Hale/Wynajem' => 'WarehouseRent'
    );
    
    public $province = array(
        'dolnośląskie' => 2,
        'kujawsko-pomorskie' => 3,
        'lubelskie' => 4,
        'lubuskie' => 5,
        'łódzkie' => 6,
        'małopolskie' => 7,
        'mazowieckie' => 8,
        'opolskie' => 9,
        'podkarpackie' => 10,
        'pomorskie' => 12,
        'śląskie' => 13,
        'podlaskie' => 11,
        'świętokrzyskie' => 14,
        'warmińsko-mazurskie' => 1,
        'wielkopolskie' => 15,
        'zachodniopomorskie' => 16
    );
    
    public $market = array(
        'pierwotny' => 2,
        'wtórny' => 1
    );
    
 
    
 
}

