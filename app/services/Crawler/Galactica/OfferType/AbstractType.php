<?php namespace Crawler\Galactica\OfferType;

class AbstractType
{
    
    protected $dom;
    protected $translator;
    
    public function getMainData() {
        $data = array();
            $data['title'] = $this->getTitle();
            $data['province'] = $this->getProvince();
            $data['distrinct'] = $this->getDistrinct();
            $data['city'] = $this->getCity();
            $data['section'] = $this->getSection();
            $data['street'] = $this->getStreet();
            $data['surface'] = $this->getSurface();
            $data['price'] = $this->getPrice();
            $data['price_negotiable'] = $this->getPriceNegotiable();
            $data['price_surface'] = $this->getPriceSurface($data['price'], $data['surface']);
            $data['description'] = $this->getDescription();

        return $data;
    }
    
    protected function getPrice() {
        $price = pq($this->dom)->find('td.od-offer-price')->clone()->children()->remove()->end()->text();
        return $this->cleanup($price, array('\s')); 
    }
    
    protected function getTitle() {
        return (string)pq($this->dom)->find('h1.offer-title')->text();
    }
    
    protected function getSurface() {
        $container = pq($this->dom)->find('td.od-offer-area')->text();
        $text =  $this->cleanup($container, array('\s' , 'm2' , 'm'));
        $text =  preg_replace('/,/', '.', $container);
        return (double) $text; 
    }
    protected function getProvince() {
        
        $body = pq($this->dom)->find('dd#mapTitle')->html();
        preg_match('/województwo:\s<a.*?><strong>(.*?)<\/strong><\/a>/', $body, $result);
        
        if ($result[1]) {
            return $this->translator->translate('province', $result[1]);
        }
    }
    
    protected function getDistrinct() {
        $body = pq($this->dom)->find('dd#mapTitle')->html();
        preg_match('/powiat:\s<strong>(.*?)<\/strong>/', $body, $result);
        return isset($result[1]) ? $result[1] : '';
    }
    
    protected function getCity() {
        
        $body = pq($this->dom)->find('dd#mapTitle')->html();
        preg_match('/miejscowość:\s<a.*?><strong>(.*?)<\/strong><\/a>/', $body, $result);
        
        return isset($result[1]) ? $result[1] : '';
    }
    
    protected function getStreet() {
        
        $body = pq($this->dom)->find('dd#mapTitle')->html();
        preg_match('/ulica:\s<a.*?><strong>(.*?)<\/strong><\/a>/', $body, $result);
        return isset($result[1]) ? $result[1] : '';
    }
    
    protected function getSection() {
        $body = pq($this->dom)->find('dd#mapTitle')->html();
        preg_match('/dzielnica:\s<strong>(.*?)<\/strong>/', $body, $result);
       

        return isset($result[1]) ? $result[1] : '';
    }
    
    protected function getPriceSurface($price, $surface) {
        
        return round($price / $surface); 
    }
    
    protected function getDescription() {
        return (string)pq($this->dom)->find('div.od-offer-description')->html();
    }
    
    protected function getPriceNegotiable() {
        return 0;
    }

    protected function rowText($text)
    {
        $text = pq($this->dom)->find('dt:contains(' . $text . ')')->next()->text(); 
        return trim($text); 
    }

    protected function mainRowText($rowName , $matches = array(), $group = false)
    {

        $text = pq($this->dom)->find('dt:contains(' . $rowName . ')')->next()->text(); 

        

        foreach($matches as $match)
        {
            if(preg_match('/' . $match . '/' , $text))
            {
                
                if(!$group) return $match; 
                else
                {
                    $matched[] = $match; 
                }
            }
        }

            if($group && !empty($matched)) return $matched; 
            else return ''; 
    }

    protected function mainRowRegex($rowName, $regex)
    {
           $text = pq($this->dom)->find('dt:contains(' . $rowName . ')')->next()->text(); 
           preg_match($regex, $text, $matches)
           return $matches; 

    }


    protected function cleanup($value , $chars = array())
    {
    	return preg_replace('/'. implode($chars, '|') . '/' , '' , $value); 
    }

}
