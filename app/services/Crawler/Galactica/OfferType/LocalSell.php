<?php
namespace Crawler\Galactica\OfferType;

use \Crawler\Galactica\OfferType\AbstractType as AbstractType;
/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LocalSell extends AbstractType
{
    
    public function __construct($oDom, $translator) {
        $this->dom = $oDom;
        $this->translator = $translator;
    }
    
    public function getData() {
        
        $data = array(
            'detail' => array() ,
            'main' => array()
        );

        $data['detail']['market'] = $this->rowText('rynek:');
        $data['detail']['purpose'] = $this->mainRowText('lokal:', $this->translator->getNames('purpose'));
        $data['detail']['building_type'] = $this->mainRowText('lokal:', $this->translator->getNames('building_type'));
        $data['detail']['entry'] = '';
        $data['detail']['height'] = '';
        $data['detail']['rooms'] = '';
        $data['detail']['floor'] = $this->floor('floor', $this->rowText('piętro:'));
        $data['detail']['levels'] = '';
        $data['detail']['floors_in_building'] = '';
        $data['detail']['year'] = $this->rowText('rok budowy:');
        $data['detail']['ownership'] = $this->mainRowText('lokal:', $this->translator->getNames('ownership'));
        $data['detail']['rent'] = '';
        $data['detail']['rent_description'] = '';
        $data['detail']['quality'] = $this->mainRowText('lokal:', $this->translator->getNames('quality'));
        $data['detail']['windows'] = '';
        $data['detail']['heat'] = '';
        $data['detail']['available'] = $this->rowText('wolny od:');
        
        foreach ($data['detail'] as $key => & $element) {
            if (!empty($element)) {
                $element = $this->translator->translate($key, $element);
            }
        }
        
        $data['main'] = $this->getMainData();
        
        return $data;
    }
}
