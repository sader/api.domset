<?php
namespace Crawler\Otodom;

use Crawler\CrawlerAbstract;

class OtodomAdapter extends CrawlerAbstract
{
    
    private $curl;
    private $translator;
    private $url = 'http://otodom.pl';
    private $listUrl;
    private $detailUrl;
    private $limitPerPage = 100;
    private $detailDom;
    private $provider  = ''; 


    public function __construct($translator, \Curl $curl) {
        $this->curl = $curl;
        $this->curl->option('BINARYTRANSFER', 1)->option('HEADER', 0);
        
        $this->translator = $translator;
    }
    
    public function getProvider()
    {
        return $this->provider;
    }
    
    public function setProvider($provider)
    {
        $this->provider = $provider;
        return $this;
    }

    
    public function getListUrl($page = 1) {
      $this->listUrl = $this->url;
        $this->listUrl.= '/biuro-nieruchomosci/';
        $this->listUrl.= $this->getProvider();
        $this->listUrl.= '/'. $page;            
        $this->listUrl.= '?objSearchQuery.Orderby=default&resultsPerPage=';
        $this->listUrl.= $this->limitPerPage;
        return $this->listUrl; 
    }
    
    public function setDetailUrl($url) {
        $this->detailUrl = $url;
    }
    
    public function getDetailUrl() {
        return $this->detailUrl;
    }
    
    public function getPageNumber() {
        $offers_num = preg_replace('/\s/', '', pq('.od-found-offers')->find('strong')->text());
        return ceil($offers_num / $this->limitPerPage);
    }
    
    public function getOffersList() {
            
        $aLinks = array();
        $data = $this->curl->simple_get($this->getListUrl());
        $oDom = $this->getDom($data);
        
        $pages = $this->getPageNumber();
        
        
        $links = pq('h1.od-listing_item-title');
        
        foreach ($links as $link) {
            $aLinks[] = $this->url . pq($link)->find('a')->attr('href');
        }
        
        if ($pages > 1) {
            for ($i = 2; $i <= $pages; $i++) {

                $data = $this->curl->simple_get($this->getListUrl($i));
                $oDom = $this->getDom($data);
                $links = pq('h1.od-listing_item-title');
                foreach ($links as $link) {
                    $aLinks[] = $this->url . pq($link)->find('a')->attr('href');
                }
            }
        }
        
        return $this->tested($aLinks);
    }
    
    public function getOfferDetails($link) {
        
        $this->setDetailUrl($link);
        $data = $this->curl->simple_get($this->getDetailUrl());
        $this->detailDom = $this->getDom($data);
        $sTypeName = $this->detectType($this->detailDom);
        
        $adapterName = "\Crawler\Otodom\OfferType\\" . $sTypeName;
        $translatorName = "\Crawler\Otodom\Translator\\" . $sTypeName;
        
        $adapter = new $adapterName($this->detailDom, new $translatorName());
        $return['data'] = $adapter->getData();
        $return['adapter'] = $sTypeName;
        
        return $return;
    }
    
    public function detectType($dom) {
        $typeName = pq($dom)->find('span.od-offer-src')->text();
        $type = false;
        
        try {
            $type = $this->translator->translate('type', $typeName, true);
        }
        catch(Exception $e) {
            $type = false;
        }
        
        return $type;
    }
    
    public function getImages() {
        
        $aImages = array();
        $aImagesSrc = json_decode(pq($this->detailDom)->find('#offerGallery')->attr('data-photos'));
        
        if (!empty($aImagesSrc)) {
            foreach ($aImagesSrc as $image) {
                $aImages[] = str_replace('93x70', '525x394', $image);
            }
        }
        
        return $aImages;
    }
    
    public function testUrl($sProviderUrl) {
    }
}

