<?php
namespace Crawler\Otodom\OfferType;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LandSell extends AbstractType
{
    
    public function __construct($oDom, $translator) {
        $this->dom = $oDom;
        $this->translator = $translator;
    }
    
    public function getData() {
        $data = array(
            'detail' => array() ,
            'main' => array()
        );
    

        $data['detail']['land_type'] = $this->mainRowText('działka:', $this->translator->getNames('land_type'));
        $data['detail']['shape'] = ''; 
        $data['detail']['width'] = $this->size($this->mainRowRegex('działka:' , '/([0-9]+)\sx\s([0-9]+)/') , 'width');
        $data['detail']['height'] = $this->size($this->mainRowRegex('działka:' , '/([0-9]+)\sx\s([0-9]+)/') , 'height');
        $data['detail']['fence'] = $this->rowText('ogrodzenie:');
        $data['detail']['access'] = $this->rowText('dojazd:');
        $data['detail']['services'] = '';
        

        $data['main'] = $this->getMainData();

          foreach ($data['detail'] as $key => &$element) {
            if (!empty($element)) {
                $element = $this->translator->translate($key, $element);
            }
        }
        return $data;
    }


    public function size($size, $type)
    {
        
        if($type == 'width' && isset($size[1]))
        {
            return $size[1]; 
        }
        elseif($type == 'height' && isset($size[2]))
        {
            return $size[2]; 
        }

        return ''; 

    }
}
