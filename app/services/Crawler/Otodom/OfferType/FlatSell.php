<?php
namespace Crawler\Otodom\OfferType;

use Offer\Translator\FlatSell as Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class FlatSell extends AbstractType
{
    
    public function __construct($oDom, $translator) {
        $this->dom = $oDom;
        $this->translator = $translator;
    }
    
    public function getData() {

        $data = array(
            'detail' => array() ,
            'main' => array()
        );
        
        $data['detail']['market'] = $this->rowText('rynek:');
        $data['detail']['rooms'] = $this->mainRowText('mieszkanie:', $this->translator->getNames('rooms'));
        $data['detail']['floor'] = $this->floor('floor', $this->rowText('piętro:'));
        $data['detail']['levels'] = '';
        $data['detail']['floors_in_building'] = $this->floor('floors_in_building', $this->rowText('piętro:'));
        $data['detail']['building_type'] = $this->mainRowText('mieszkanie:', $this->translator->getNames('building_type'));
        $data['detail']['year'] = $this->rowText('rok budowy:');
        $data['detail']['building_material'] = $this->rowText('materiał budynku:');
        $data['detail']['ownership'] = $this->mainRowText('mieszkanie:', $this->translator->getNames('ownership'));
        $data['detail']['rent'] = $this->rowText('czynsz:');
        $data['detail']['quality'] = $this->rowText('stan wykończenia:');
        $data['detail']['installation_quality'] = '';
        $data['detail']['windows'] = $this->mainRowText('mieszkanie:', $this->translator->getNames('windows'));
        $data['detail']['kitchen'] = '';
        $data['detail']['noise'] = '';
        $data['detail']['heat'] = $this->mainRowText('mieszkanie:', $this->translator->getNames('heat'));
        
        foreach ($data['detail'] as $key => &$element) {
            if (!empty($element)) {
                $element = $this->translator->translate($key, $element);
            }
        }
        
        
        $data['main'] = $this->getMainData();
        
        return $data;
    }
    
    public function floor($get, $text) {
        
        if (preg_match('/([0-9]+)\s*?\/\s*?([0-9]+)/', $text, $matches)) {
            
            $result = array(
                'floor' => $matches[1],
                'floors_in_building' => $matches[2]
            );
        } else {
            $result = array(
                'floor' => $text,
                'floors_in_building' => ''
            );
        }
        
        return isset($result[$get]) ? $result[$get] : '';
    }
}
