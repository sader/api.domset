<?php
namespace Crawler\Otodom\OfferType;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class HouseSell extends AbstractType
{
    
    public function __construct($oDom, $translator) {
        $this->dom = $oDom;
        $this->translator = $translator;
    }
    
    public function getData() {
        

        $data = array(
            'detail' => array() ,
            'main' => array()
        );
        
        $data['detail']['market'] = $this->rowText('rynek:');
        $data['detail']['house_surface'] = '';
        $data['detail']['terrain_surface'] = $this->rowText('powierzchnia działki:'); 
        $data['detail']['building_type'] = $this->mainRowText('dom:', $this->translator->getNames('building_type'));
        $data['detail']['rooms'] = $this->mainRowText('dom:', $this->translator->getNames('rooms'));
        $data['detail']['floors'] = $this->mainRowText('dom:', $this->translator->getNames('levels'));
        $data['detail']['attic'] = $this->mainRowText('dom:', $this->translator->getNames('garret'));
        $data['detail']['basement'] = '';
        $data['detail']['year'] = $this->rowText('rok budowy:');
        $data['detail']['building_material'] = $this->rowText('materiał budynku:'); 
        $data['detail']['quality'] = $this->mainRowText('dom:', $this->translator->getNames('quality'));
        $data['detail']['windows'] = $this->mainRowText('dom:', $this->translator->getNames('windows'));
        $data['detail']['roof'] = $this->mainRowText('dom:', $this->translator->getNames('roof'));;
        $data['detail']['roof_type'] = '';
        $data['detail']['heat'] = $this->mainRowText('dom:', $this->translator->getNames('heat'));
        $data['detail']['sewerage'] = '';
        $data['detail']['fence'] = $this->mainRowText('dom:', $this->translator->getNames('fence'));;
        $data['detail']['garage'] = '';
        $data['detail']['access'] =  $this->rowText('dojazd:'); 
        
        $data['main'] = $this->getMainData();

          foreach ($data['detail'] as $key => &$element) {
            if (!empty($element)) {
                $element = $this->translator->translate($key, $element);
            }
        }
        
        return $data;
    }

        
}
