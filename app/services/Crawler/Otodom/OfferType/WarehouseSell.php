<?php
namespace Crawler\Otodom\OfferType;
use Crawler\Otodom\OfferType\AbstractType; 
/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class WarehouseSell extends AbstractType
{
    
    public function __construct($oDom, $translator) {
        $this->dom = $oDom;
        $this->translator = $translator;
    }
    
    public function getData() {
        
        $data['detail']['market`'] = $this->rowText('rynek:');
        $data['detail']['purpose'] = $this->mainRowText('hala/magazyn:', $this->translator->getNames('purpose'));
        $data['detail']['rooms'] = '';
        $data['detail']['height'] = '';
        $data['detail']['floor'] = $this->mainRowText('hala/magazyn:', $this->translator->getNames('purpose'));
        $data['detail']['levels'] = '';
        $data['detail']['year'] = $this->rowText('rok budowy:');
        $data['detail']['quality'] = $this->mainRowText('hala/magazyn:', $this->translator->getNames('quality'));
        $data['detail']['heat'] = $this->mainRowText('hala/magazyn:', $this->translator->getNames('heat'));
        
      foreach ($data['detail'] as $key => & $element) {
            if (!empty($element)) {
                $element = $this->translator->translate($key, $element);
            }
        }
        
        $data['main'] = $this->getMainData();
        
        return $data;
    }
}
