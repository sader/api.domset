<?php namespace Crawler\Otodom\Translator;

use Offer\Translator\TranslatorAbstract;


class Core extends TranslatorAbstract
{
    
    public $type = array(
        'mieszkanie na sprzedaż' => 'FlatSell',
        'mieszkanie na wynajem' => 'FlatRent',
        'dom na sprzedaż' => 'HouseSell',
        'dom na wynajem' => 'HouseRent',
        'działka na sprzedaż' => 'LandSell',
        'działka na wynajem' => 'LandRent',
        'lokal użytkowy na sprzedaż' => 'LocalSell',
        'lokal użytkowy na wynajem' => 'LocalRent',
        'hala/magazyn na sprzedaż' => 'WarehouseSell',
        'hala/magazyn na wynajem' => 'WarehouseRent'
    );
    
    public $province = array(
        'dolnośląskie' => 2,
        'kujawsko-pomorskie' => 3,
        'lubelskie' => 4,
        'lubuskie' => 5,
        'łódzkie' => 6,
        'małopolskie' => 7,
        'mazowieckie' => 8,
        'opolskie' => 9,
        'podkarpackie' => 10,
        'pomorskie' => 12,
        'śląskie' => 13,
        'podlaskie' => 11,
        'świętokrzyskie' => 14,
        'warmińsko-mazurskie' => 1,
        'wielkopolskie' => 15,
        'zachodniopomorskie' => 16
    );
    
    public $market = array(
        'pierwotny' => 2,
        'wtórny' => 1
    );
    
 
    
 
}

