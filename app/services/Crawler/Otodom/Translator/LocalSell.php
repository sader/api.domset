<?php namespace Crawler\Otodom\Translator;
/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LocalSell extends Core {

public $purpose = array(
    'usługowy' => 2,
    'biurowy' => 1,
    'handlowy' => 2,
    'gastronomiczny' => 3,
    'przemysłowy' => 4,
    'hotelowy' => 5); 


 public $additional = array(
    'witryna' => 1,
    'winda' => 7,
    'klimatyzacja' => 8);


    public $buliding_type = array(

    'w centrum handlowym' => 1,
    'w biurowcu' => 2,
    'w bloku' => 3,
    'w kamienicy' => 4,
    'w domu prywatnym' => 3,
    'w budynku zabytkowym' => 13,
    'osobny obiekt' => 12); 


    public $quality = array(
    'gotowy' => 1,
    'do wykończenia' => 1,
    'do remontu' => 1);
		
}