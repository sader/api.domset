<?php namespace Crawler\Otodom\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class FlatSell extends Core
{
    
    public $building_type = array(
        'blok' => 1,
        'kamienica' => 2,
        'dom wolnostojący' => 3,
        'plomba' => 4,
        'szeregowiec' => 5,
        'apartamentowiec ' => 6,
        'loft' => 7
    );
    
    public $rooms = array(
        '1-pokojowe' => 1,
        '2-pokojowe' => 2,
        '3-pokojowe' => 3,
        '4-pokojowe' => 4,
        '5-pokojowe' => 5,
        '6-pokojowe' => 6,
        '7-pokojowe' => 7
    );
    

    public $levels = array(
        '1 piętro' => 1,
        '2 piętra' => 2,
        '3 piętra' => 3,
        '4 piętra' => 4,
    );


    public $building_material = array(
        'cegła' => 2,
        'drewno' => 5,
        'pustak' => 3,
        'keramzyt' => 7,
        'wielka płyta' => 1,
        'beton' => 4,
        'inne' => 7,
        'silikat' => 7,
        'beton komórkowy' => 7
    );
    
    public $ownership = array(
        'spółdzielcze własnościowe. z KW' => 3,
        'spółdzielcze własnościowe' => 2,
        'pełna własność' => 1,
        'udział' => 4
    );
    
    public $windows = array(
        
        'plastikowe' => 1,
        'drewniane' => 2,
        'aluminiowe' => 3
    );
    
    public $quality = array(
        'do zamieszkania' => 1,
        'do wykończenia' => 2,
        'do remontu' => 3
    );
    
    public $heat = array(
        
        'miejskie' => 1,
        'gazowe' => 2,
        'piece kaflowe' => 3,
        'elektryczne' => 4,
        'inne',
    );
    
    public $additional = array(
        'garaż' => 4,
        'piwnica' => 6,
        'ogródek' => 17,
        'taras' => 18,
        'winda' => 7,
        'klimatyzacja' => 10
    );
    
    public $heating = array(
        
        'miejskie' => 9,
        'gazowe' => 1,
        'piece kaflowe' => 11,
        'elektryczne' => 8,
        'inne' => 12,
        'kotłownia' => 12
    );
}
