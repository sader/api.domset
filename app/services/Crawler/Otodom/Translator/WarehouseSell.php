<?php namespace Crawler\Otodom\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class WarehouseSell extends Core
{
    
    public $access = array(
        'utwardzony betonowy' => 1,
        'nieutwardzony' => 1,
        'asfalt' => 1
    );
    
    public $media = array(
        'woda' => 12,
        'siła' => 13,
        'kanalizacja' => 14,
        'telefon' => 7,
        'internet' => 8
        );
    
    public $purpose = array(
        'magazynowe' => 2,
        'produkcyjne' => 3,
        'biurowe' => 1,
        'handlowe' => 4
    );
    
    public $access = array(
        'brak' => 1,
        'utwardzony' => 1,
        'betonowy' => 1,
        'asfaltowy' => 1,
        'nieutwardzony' => 1,
        'kostka brukowa' => 1
    );
    
    public $floor = array(
        'brak' => 1,
        'pylna' => 1,
        'niepylna' => 1
    );
    
    public $quality = array(
        'gotowe' => 3,
        'do wykończenia' => 7,
        'do remontu' => 6,
        'stan surowy otwarty' => 3,
        'stan surowy zamknięty' => 3
    );
    
    public $fence = array(
        'tak' => 1,
        'nieogrodzony' => 1
    );

    public  $heat  = array(
        'tak' => 1,
        'nie' => 0

        ); 
}
