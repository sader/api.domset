<?php namespace Crawler;

class CrawlerAbstract
{
    
	const TYPE_OTODOM = 2;
	const TYPE_PARTNER_SITE = 3; 
	protected $tester = ''; 

    public function getDom($htmlData)
    {
           $htmlData = mb_convert_encoding($htmlData, 'HTML-ENTITIES', "UTF-8");
        return \phpQuery::newDocumentHTML($htmlData, 'utf-8');
    }

    public function getTester()
    {
        return $this->tester;
    }
    
    public function setTester($tester)
    {
        $this->tester = $tester;
        return $this;
    }

    public function isTester()
    {
    	return !empty($this->tester); 
    }


    public function tested($urlList)
    {
    	if($this->isTester())
    	{
                $key = array_search($this->getTester(), $urlList); 
                if ($key === false)
                {
                    throw new \Exception('Tester is set but url '.$this->getTester().' is not found in '.print_r($urlList)); 
                }
                else return array($this->getTester()); 
    	}
    	else
    	{
    		return $urlList;
    	}
    }


}

