<?php

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('curl', function()
        {
            return new Curl;
        });

        $this->app->bind('gallery', function()
        {
            return new \Gallery\Manager(new Curl);
        });

	
       $this->app->bind('otodom_crawler', function()
        {
            return new \Crawler\Otodom\OtodomAdapter(new \Crawler\Otodom\Translator\Core, new Curl);
        });

        $this->app->bind('galactica_crawler', function()
        {
            return new \Crawler\Galactica\GalacticaAdapter(new \Crawler\Galactica\Translator\Core, new Curl);
        });

    }

}