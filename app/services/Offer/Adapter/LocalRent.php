<?php namespace Offer\Adapter;
/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LocalRent extends CoreAdapter {

	 public function __construct() {
        $this->setTranslator(new \Offer\Translator\LocalRent());
        $this->setModel('OfferLocalRent');  
        $this->setId(8); 
        $this->setViewTemplate('offer.list.local_rent'); 
    }
}