<?php namespace Offer\Adapter;
/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LocalSell extends CoreAdapter {


	 public function __construct() {
        $this->setTranslator(new \Offer\Translator\LocalSell());
        $this->setModel('OfferLocalSell'); 
        $this->setId(7); 
        $this->setViewTemplate('offer.list.lacal_sell'); 
    }
}