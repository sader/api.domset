<?php
namespace Offer\Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class WarehouseSell extends CoreAdapter
{
    
    public function __construct() {
        $this->setTranslator(new \Offer\Translator\WarehouseSell());
        $this->setModel('OfferWarehouseSell');
        $this->setId(10);
     $this->setViewTemplate('offer.list.warehouse_sell'); 
    }
}
