<?php
namespace Offer\Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class CoreAdapter
{
    
    private $translator;
    private $name;
    private $model;
    private $viewTemplate;
    
    protected $adapterNames = array(
        1 => 'Mieszkania sprzedaż',
        2 => 'Mieszkania wynajem',
        3 => 'Domy sprzedaż',
        4 => 'Domy wynajem',
        5 => 'Działki sprzedaż',
        6 => 'Działki wynajem',
        7 => 'Lokale sprzedaż',
        8 => 'Lokale wynajem',
        9 => 'Magazyny sprzedaż',
        10 => 'Magazyny wynajem'
    );
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->adapterNames[$this->getId() ];
    }
    
    public function getNameById($id) {
        return $this->adapterNames[$id];
    }
    
    public function setTranslator($translator) {
        $this->translator = $translator;
    }
    
    public function getTranslator() {
        return $this->translator;
    }
    
    public function translate($key, $value) {
        
        return $this->translator->translate($key, $value);
    }
    
    public function get($key) {
        return $this->translator->get($key);
    }
    
    public function getModel() {
        return $this->model;
    }
    
    public function setmodel($modelName) {
        $this->model = $modelName;
    }
    
    public function getViewTemplate() {
        return $this->viewTemplate;
    }
    
    public function setViewTemplate($viewTemplate) {
        $this->viewTemplate = $viewTemplate;
        return $this;
    }
    
    public function updateOrCreate($data, $link) {
        

        $offerDetail = \Offer\OfferFactory::createModelByName($data['adapter']);
        


        try {
            
            \DB::beginTransaction();
            
            $offer = \Offer::where("api_id" , $link)->first();
            
            if ($offer) {
                $offer->update($data['data']['main']);
                $offerDetail::find($offer->id)->update($data['data']['detail']);
                \Event::fire('offer.reloaded', $offer->id);

            } else {

                $offer = \Offer::create($data['data']['main']);
                $data['data']['detail']['id'] = $offer->id;
                $added = $offerDetail::create($data['data']['detail']);
                \Event::fire('offer.added', $offer->id);
            }

            \DB::commit();

        }

        catch(Exception $e) {
            
            \DB::rollback();
            dd($e->getMessage());
        }

          return $offer->id; 
    }
}
