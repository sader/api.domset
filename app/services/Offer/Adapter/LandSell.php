<?php namespace Offer\Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LandSell extends CoreAdapter {

	 public function __construct() {
        $this->setTranslator(new \Offer\Translator\LandSell());
        $this->setModel('OfferLandSell');
        $this->setId(5);  
        $this->setViewTemplate('offer.list.local_sell'); 
    }
}