<?php namespace Offer\Adapter; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class WarehouseRent extends CoreAdapter {

	 public function __construct() {
        $this->setTranslator(new \Offer\Translator\WarehouseRent());
        $this->setModel('OfferWarehouseRent');
        $this->setId(10); 
        $this->setViewTemplate('offer.list.warehouse_rent'); 
    }
}