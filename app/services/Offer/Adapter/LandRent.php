<?php namespace Offer\Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LandRent extends CoreAdapter {

	 public function __construct() {
        $this->setTranslator(new \Offer\Translator\LandRent());
        $this->setModel('OfferLandRent');
        $this->setId(6); 
        $this->setViewTemplate('offer.list.land_rent');  
    }
}