<?php namespace Offer\Adapter; 

use Offer\Translator\FlatSell as Adapter; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class FlatSell extends CoreAdapter {


	public function __construct()
	{
		$this->setTranslator(new \Offer\Translator\FlatSell()); 
		$this->setModel('OfferFlatSell');
		$this->setId(1); 
		$this->setViewTemplate('offer.list.flat_sell'); 
	}

}