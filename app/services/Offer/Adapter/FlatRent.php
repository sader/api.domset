<?php namespace Offer\Adapter; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class FlatRent extends CoreAdapter {

	public function __construct()
	{
		$this->setTranslator(new \Offer\Translator\FlatRent()); 
		$this->setModel('OfferFlatRent');  
		$this->setId(2); 
		$this->setViewTemplate('offer.list.flat_rent'); 
	}


}