<?php namespace Offer\Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class HouseRent extends CoreAdapter {


	public function __construct()
	{
		$this->setTranslator(new \Offer\Translator\HouseRent()); 
		$this->setModel('OfferHouseRent');
		$this->setId(4); 
		$this->setViewTemplate('offer.list.house_rent'); 
	}
}