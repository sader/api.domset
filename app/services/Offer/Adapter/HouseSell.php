<?php
namespace Offer\Adapter;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class HouseSell extends CoreAdapter
{
    
    public function __construct() {
        $this->setTranslator(new \Offer\Translator\HouseSell());
        $this->setModel('OfferHouseSell');
        $this->setId(3); 
        $this->setViewTemplate('offer.list.house_sell'); 
    }
}
