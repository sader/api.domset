<?php
namespace Offer;

// use Offer\Adapter as Adapter;

class OfferFactory
{
    
   
    private static $registeredAdapters = array(
        1 => 'FlatSell',
        2 => 'FlatRent',
        3 => 'HouseSell',
        4 => 'HouseRent',
        5 => 'LandSell',
        6 => 'LandRent',
        7 => 'LocalSell',
        8 => 'LocalRent',
        9 => 'WarehouseSell',
        10 => 'WarehouseRent'
    );


    public static function getIdByName($adapter_name)
    {
        $flipped = array_flip(self::$registeredAdapters); 
        return isset($flipped[$adapter_name]) ? $flipped[$adapter_name] : ''; 


        
    }
    
    public static function create($type) {

        $adatpers = self::$registeredAdapters;
        
        if (isset($adatpers[$type])) {
            $className = "Offer\Adapter\\" . $adatpers[$type];
            return new $className();
            
        } else {
            throw new \Exception('No adapter found' , 500);
        }
    }

    public static function createModelByName($sName)
    {
        $objectName = "Offer" . $sName; 
        return new $objectName;
    }

   
}
