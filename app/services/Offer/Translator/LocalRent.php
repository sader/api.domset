<?php namespace Offer\Translator; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LocalRent extends LocalSell {

  public $purpose = array(
        1 => 'Biuro',
        2 => 'Handel i usługi',
        3 => 'Gastronomia',
        4 => 'Przemysł',
        5 => 'Inne'
    );

   public $type = array(
        1 => 'Centrum handlowe',
        2 => 'Biurowiec',
        3 => 'Blok',
        4 => 'Kamienica',
        5 => 'Pawilon',
        6 => 'Kiosk',
        7 => 'Sklep',
        8 => 'Hotel',
        9 => 'Kawiarnia',
        10 => 'Restauracja',
        11 => 'Gabinet',
        12 => 'Oddzielny obiekt',
        13 => 'Inny'
    );

       public $additional = array(
        1 => 'Witryna',
        2 => 'Ochrona',
        3 => 'Monitoring',
        4 => 'System alarmowy',
        5 => 'Piwnica',
        6 => 'Zaplecze',
        7 => 'Winda',
        8 => 'Klimatyzacja',
        9 => 'Telefon',
        10 => 'Internet',
        11 => 'Telewizja kablowa',
        12 => 'Duży parking',
        13 => 'Rampa'
    );

   
  

}