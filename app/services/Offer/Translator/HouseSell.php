<?php  namespace Offer\Translator; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class HouseSell extends TranslatorAbstract {

	 public $heat = array(
        1 => 'Gazowe',
        2 => 'Węglowe',
        3 => 'Biomasa',
        4 => 'Pompa ciepła',
        5 => 'Kolektor słoneczny',
        6 => 'Olejowe',
        7 => 'Geotermika',
        8 => 'Elektryczne',
        9 => 'Miejskie',
        10 => 'Kominkowe',
        11 => 'Piece kaflowe',
        12 => 'Inne'
    );

	  public $type = array(
        1 => 'Wolnostojący',
        2 => 'Segment środkowy',
        3 => 'Segment skrajny',
        4 => 'Bliźniak',
        5 => 'Kamienica',
        6 => 'Dworek',
        7 => 'Letniskowy',
        8 => 'Gospodarstwo'
    );

	   public $additional = array(
        1 => 'Klimatyzacja',
        2 => 'Ogrzewanie podłogowe',
        3 => 'Telefon',
        4 => 'Internet',
        5 => 'Telewizja kablowa',
        6 => 'Kominek',
        7 => 'Ogródek',
        8 => 'Taras',
        9 => 'Strych',
        10 => 'Woda miejska',
        11 => 'Zagospodarowany ogród',
        12 => 'Oczko wodne',
        13 => 'W pobliżu las',
        14 => 'W pobliżu jezioro',
        15 => 'Dostęp do jeziora'
    );

       public $building_material = array(
        1 => 'Cegła',
        2 => 'Pustak',
        3 => 'Płyta',
        3 => 'Bloczki',
        4 => 'Drewno',
        5 => 'Silikat',
        6 => 'Mieszany',
        7 => 'Inny');

}