<?php
namespace Offer\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class HouseRent extends HouseSell
{
    
   
    
    public $type = array(
        1 => 'Wolnostojący',
        2 => 'Segment środkowy',
        3 => 'Segment skrajny',
        4 => 'Bliźniak',
        5 => 'Kamienica',
        6 => 'Dworek',
        7 => 'Letniskowy',
        8 => 'Gospodarstwo'
    );
    
    public $additional = array(
        1 => 'Klimatyzacja',
        2 => 'Ogrzewanie podłogowe',
        3 => 'Telefon',
        4 => 'Internet',
        5 => 'Telewizja kablowa',
        6 => 'Kominek',
        7 => 'Ogródek',
        8 => 'Taras',
        9 => 'Strych',
        10 => 'Woda miejska',
        11 => 'Zagospodarowany ogród',
        12 => 'Oczko wodne',
        13 => 'W pobliżu las',
        14 => 'W pobliżu jeziora',
        15 => 'Dostęp do jeziora',
        16 => 'Dom umeblowany'
    );
}
