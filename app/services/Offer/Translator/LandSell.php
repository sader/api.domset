<?php namespace Offer\Translator; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class LandSell extends TranslatorAbstract {

	 public $additional = array(
        1 => 'Działka z linią brzegową jeziora',
        2 => 'Działka z dostępem do jeziora',
        3 => 'Działka przy rzece',
        4 => 'Blisko jeziora',
        5 => 'Blisko lasu'
    );

	   protected $land_type = array(
        1 => 'budowlana',
        2 => 'rolna',
        3 => 'usługowa',
        4 => 'przemysłowa',
        5 => 'inwestycyjna',
        6 => 'rekreacyjna',
        7 => 'leśna',
        8 => 'gospodarstwo',
        9 => 'siedlisko',
        10 => 'rolno-budowlana',
        11 => 'inna'
    );
    protected $shape = array(
        1 => 'Prostokąt',
        2 => 'Kwadrat',
        3 => 'Romb',
        4 => 'Trapez',
        5 => 'Trójkąt',
        6 => 'Inny'
    );
}