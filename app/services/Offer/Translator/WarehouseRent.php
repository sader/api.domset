<?php namespace Offer\Translator; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class WarehouseRent extends TranslatorAbstract {

	protected $purpose = array(
        1 => 'Biurowe',
        2 => 'Magazynowe',
        3 => 'Produkcyjne',
        4 => 'Handlowe',
        5 => 'Inne'
    );
    
    protected $additional = array(
        1 => 'Ochrona',
        2 => 'Monitoring',
        3 => 'System alarmowy',
        4 => 'Zaplecze socjalne',
        5 => 'Pomieszczenia biurowe',
        6 => 'Domofon',
        7 => 'Telefon',
        8 => 'Internet',
        9 => 'Telewizja kablowa',
        10 => 'Duży parking',
        11 => 'Rampa',
        12 => 'Woda',
        13 => 'Siła',
        14 => 'Kanalizacja',
        15 => 'Klimatyzacja',
        16 => 'Teren ogrodzony'
    );

    protected $heat = array(
        1 => 'Miejskie',
        2 => 'Elektryczne',
        3 => 'Gazowe',
        4 => 'Piec',
        5 => 'Kotłownia',
        6 => 'Kominkowe',
        7 => 'Inne',
        8 => 'Brak');

}