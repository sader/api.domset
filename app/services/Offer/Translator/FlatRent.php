<?php namespace Offer\Translator; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class FlatRent extends TranslatorAbstract {

   public $material_type = array(
        1 => 'Wielka płyta',
        2 => 'Cegła',
        3 => 'Pustak',
        4 => 'Beton',
        5 => 'Drewno',
        6 => 'Ytong',
        7 => 'Inny');

     public $heat = array(
        1 => 'Miejskie',
        2 => 'Elektryczne',
        3 => 'Gazowe',
        4 => 'Piec',
        5 => 'Kotłownia',
        6 => 'Kominkowe',
        7 => 'Inne');

     public $additional = array(
        1 => 'Osiedle zamknięte',
        2 => 'Podziemny garaż',
        3 => 'Garaż',
        4 => 'Duży parking',
        5 => 'Piwnica',
        6 => 'Winda',
        7 => 'Mieszkanie umeblowane',
        8 => 'Klimatyzacja',
        9 => 'Ogrzewanie podłogowe',
        10 => 'Telefon',
        11 => 'Internet',
        12 => 'Telewizja kablowa',
        13 => 'Kominek',
        14 => 'Ogródek',
        15 => 'Taras',
        16 => 'Strych'
    );

}