<?php
namespace Offer\Translator;

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class TranslatorAbstract
{
    public $type = null;
    public function translate($fieldName, $value, $required = false) {
       
        if(isset($this->$fieldName))
        {
            $element = $this->$fieldName;           
        }
        else
        {
            if ($required) {
                throw new \Exception($fieldName . ' - not found for ' . $value, 500);
            } else {
                return $value;
            }
        }
        
        
        if (is_array($value)) {
            return array_values(array_intersect_key($element, array_flip($value)));
        } else if (isset($element) and isset($element[$value])) {
            return $element[$value];
        } else {
            if ($required) {
                throw new \Exception($fieldName . ' - not found for ' . $value, 500);
            } else {
                return $value;
            }
        }
    }
    
    public function getList($listName) {
        if (isset($this->$listName)) {
            return $this->$listName;
        }
    }

    public function getNames($field)
    {
        $element = $this->$field; 
        return isset($element) ? array_keys($element) : array(); 
    }

    protected $market = array(
        1 => 'Wtórny',
        2 => 'Pierwotny'
    );
    
    protected $province = array(
        1 => 'warmińsko-mazurskie',
        2 => 'dolnośląskie',
        3 => 'kujawsko-pomorskie',
        4 => 'lubelskie',
        5 => 'lubuskie',
        6 => 'łódzkie',
        7 => 'małopolskie',
        8 => 'mazowieckie',
        9 => 'opolskie',
        10 => 'podkarpackie',
        11 => 'podlaskie',
        12 => 'pomorskie',
        13 => 'śląskie',
        14 => 'świętokrzyskie',
        15 => 'wielkopolskie',
        16 => 'zachodniopomorskie'
    );
    
    protected $levels = array(
        1 => 'Jednopoziomowe',
        2 => 'Dwupoziomowe'
    );
    
    protected $building_type = array(
        1 => 'Blok',
        2 => 'Kamienica',
        3 => 'Wieżowiec',
        4 => 'Apartamentowiec',
        5 => 'Dom wielorodzinny',
        6 => 'Inny'
    );
    
    protected $ownership = array(
        1 => 'Pełna własność',
        2 => 'Społdzielcze własnościowe',
        3 => 'Spółdzielcze własnościowe z kw',
        4 => 'Udział',
        5 => 'Inne'
    );
    
    protected $quality = array(
        1 => 'Wysoki standard',
        2 => 'Bardzo dobry',
        3 => 'Dobry',
        4 => 'Do odświeżenia',
        5 => 'Do odnowienia',
        6 => 'Do remontu',
        7 => 'Do wykończenia'
    );
    
    protected $installation_state = array(
        1 => 'Nowa',
        2 => 'Po wymianie',
        3 => 'Po częściowej wymianie',
        4 => 'Do wymiany'
    );
    
    protected $windows = array(
        1 => 'Nowe plastikowe',
        2 => 'Nowe drewniane',
        3 => 'Plastikowe',
        4 => 'Drewniane',
        5 => 'Stare plastikowe',
        6 => 'Stare drewniane',
        7 => 'Aluminiowe'
    );
    
    protected $kitchen = array(
        1 => 'Kuchnia',
        2 => 'Aneks kuchenny'
    );
    
    protected $noise = array(
        1 => 'Ciche',
        2 => 'Umiarkowanie ciche',
        3 => 'Umiarkowanie głośne',
        4 => 'Głośne'
    );
    
    protected $available = array(
        1 => 'Od zaraz',
        2 => 'Za miesiąc',
        3 => 'Za 2 miesiące',
        4 => 'Za 3 miesiące',
        5 => 'Za pół roku'
    );
    
    protected $rent = array(
        1 => 'Płatny dodatkowo',
        2 => 'W cenie najmu',
        3 => 'Tylko opłaty eksploatacyjne',
        4 => 'Tylko prąd'
    );
    protected $attic = array(
        1 => 'Użytkowe',
        2 => 'Nieużytkowe',
        3 => 'Brak'
    );
    protected $basement = array(
        1 => 'Całościowe',
        2 => 'Częściowe',
        3 => 'Brak'
    );
    protected $roof = array(
        1 => 'Płaski',
        2 => 'Dwuspadowy',
        3 => 'Wielospadowy'
    );
    protected $roof_type = array(
        1 => 'Papa',
        2 => 'Dachówka',
        3 => 'Blachodachówka',
        4 => 'Blacha',
        5 => 'Inne'
    );
    
    protected $sewerage = array(
        1 => 'Miejska',
        2 => 'Szambo',
        3 => 'Przydomowa oczyszczalnia ścieków',
        4 => 'Brak'
    );
    protected $fence = array(
        1 => 'Brak',
        2 => 'Siatka',
        3 => 'Murowane',
        4 => 'Drewniane',
        5 => 'Betonowe',
        6 => 'Żywopłot',
        7 => 'Inne'
    );
    protected $garage = array(
        1 => 'Brak',
        2 => 'W budynku',
        3 => 'Wolnostojący',
        4 => 'Wiata'
    );
    protected $access = array(
        1 => 'Utwardzany',
        2 => 'Nieutwardzany',
        3 => 'Asfaltowy',
        4 => 'Droga polna'
    );
  
    protected $services = array(
        1 => 'Kanalizacja',
        2 => 'Szabmo',
        3 => 'Oczyszczalnia',
        4 => 'Wodociąg',
        5 => 'Studnia',
        6 => 'Prąd',
        7 => 'Gaz'
    );
    
    protected $entry = array(
        1 => 'Od ulicy',
        2 => 'Z klatki schodowej',
        3 => 'Od podwórza'
    );
    
     public $heat = array(
        1 => 'Gazowe',
        2 => 'Węglowe',
        3 => 'Biomasa',
        4 => 'Pompa ciepła',
        5 => 'Kolektor słoneczny',
        6 => 'Olejowe',
        7 => 'Geotermika',
        8 => 'Elektryczne',
        9 => 'Miejskie',
        10 => 'Kominkowe',
        11 => 'Piece kaflowe',
        12 => 'Inne'
    );
}
