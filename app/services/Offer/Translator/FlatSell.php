<?php namespace Offer\Translator; 

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class FlatSell extends TranslatorAbstract {


	  public $material_type = array(
        1 => 'Wielka płyta',
        2 => 'Cegła',
        3 => 'Pustak',
        4 => 'Beton',
        5 => 'Drewno',
        6 => 'Ytong',
        7 => 'Inny');

	  public $additional = array(
        1 => 'Wspólnota mieszkaniowa',
        2 => 'Osiedle zamknięte',
        3 => 'Podziemny garaż',
        4 => 'Garaż',
        5 => 'Duży parking',
        6 => 'Piwnica',
        7 => 'Winda',
        8 => 'Mieszkanie umeblowane',
        9 => 'Meble w zabudowie',
        10 => 'Klimatyzacja',
        11 => 'Ogrzewanie podłogowe',
        12 => 'Gaz',
        13 => 'Telefon',
        14 => 'Internet',
        15 => 'Telewizja kablowa',
        16 => 'Kominek',
        17 => 'Ogródek',
        18 => 'Taras',
        19 => 'Strych');
    


}