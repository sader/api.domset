<?php
namespace Gallery;

class Manager
{
    
    private $rootPath = '/images/gallery/';
    private $galleryDir;
    
    public function __construct(\Curl $curl) {
        
        $this->curl = $curl;
    }
    
    public function getGalleryDir() {
        return $this->galleryDir;
    }
    
    public function setGalleryDir($offerId) {
        $this->galleryDir = \public_path() .  $this->rootPath .  $offerId;
        return $this;
    }
    
    public function getNextFilename($loop) {
        (string) $basename = $loop + 1;
        return $basename . '.jpg';
    }
    
    public function saveGallery($images, $id_offer) {
        
        $this->setGalleryDir($id_offer); 

        if(\File::isDirectory($this->getGalleryDir()))
        {
          \File::cleanDirectory($this->getGalleryDir());     
        }   
        else 
        {
            \File::makeDirectory($this->getGalleryDir());     
        }

        $this->setGalleryDir($id_offer);
       
                
        $imgNo = 1; 


        foreach ($images as $iKey => $image) {

            $file_name = (string)$imgNo . '.jpg';
            \File::put($this->getGalleryDir() . '/' . $this->getNextFilename($iKey) , 
            		  $this->curl->simple_get($image));

            $imgNo++; 
        }
    }
}
