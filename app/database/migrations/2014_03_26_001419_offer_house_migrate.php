<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferHouseMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers_house_sell', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('market'); 
    		$table->decimal('house_surface' , 8 , 2);
    		$table->decimal('terrain_surface' , 8 , 2);
    		$table->tinyInteger('building_type'); 
    		$table->tinyInteger('rooms'); 
    		$table->tinyInteger('floors');  
    		$table->tinyInteger('attic'); 
    		$table->tinyInteger('basement'); 
    		$table->integer('year'); 
			$table->tinyInteger('building_material'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('windows'); 
    		$table->tinyInteger('roof'); 
    		$table->tinyInteger('roof_type'); 
    		$table->tinyInteger('heat'); 
    		$table->tinyInteger('sewerage'); 
    		$table->tinyInteger('fence'); 
    		$table->tinyInteger('garage');
    		$table->tinyInteger('access');

    		
    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_house_sell` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");

	
				Schema::create('offers_house_rent', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->string('available'); 
    		$table->decimal('house_surface' , 8 , 2);
    		$table->decimal('terrain_surface' , 8 , 2);
    		$table->tinyInteger('building_type'); 
    		$table->tinyInteger('rooms'); 
    		$table->tinyInteger('floors');  
    		$table->tinyInteger('attic'); 
    		$table->tinyInteger('basement'); 
    		$table->integer('year'); 
			$table->tinyInteger('building_material'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('windows'); 
    		$table->tinyInteger('roof'); 
    		$table->tinyInteger('roof_type'); 
    		$table->tinyInteger('heat'); 
    		$table->tinyInteger('sewerage'); 
    		$table->tinyInteger('fence'); 
    		$table->tinyInteger('garage');
    		$table->tinyInteger('access');
    		
    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');

      			  
		});	

		DB::statement("ALTER TABLE `offers_house_rent` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers_house_sell');
		Schema::dropIfExists('offers_house_rent');
	}

}
