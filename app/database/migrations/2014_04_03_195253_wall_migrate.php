<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WallMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('api', function($table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
    		$table->string('type'); 
    		$table->text('otodom_url');
    		$table->text('site_url');
    		$table->text('template_type');

    		$table->timestamps();

    		$table->foreign('user_id')
      			  ->references('id')->on('users')
      			  ->onDelete('cascade');
      	});

		Schema::create('walls', function($table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
    		$table->string('title'); 
    		$table->string('description');
    		$table->string('image')->nullable(); 
    		$table->timestamps();

    		$table->foreign('user_id')
      			  ->references('id')->on('users')
      			  ->onDelete('cascade');
      	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('walls');
		Schema::dropIfExists('api');
	}

}
