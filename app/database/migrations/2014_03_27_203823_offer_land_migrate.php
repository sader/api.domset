<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferLandMigrate extends Migration {

		/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers_land_sell', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('land_type'); 
    		$table->tinyInteger('shape'); 
    		$table->string('width');
    		$table->string('height');
    		$table->tinyInteger('fence');
    		$table->tinyInteger('access');

    		
    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_land_sell` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `offers_land_sell` ADD COLUMN `services` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");
	

		Schema::create('offers_land_rent', function($table)
		{
    	

			$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('land_type'); 
    		$table->tinyInteger('shape'); 
    		$table->string('width');
    		$table->string('height');
    		$table->tinyInteger('fence');
    		$table->tinyInteger('access');

    		
    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_land_rent` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");
			DB::statement("ALTER TABLE `offers_land_rent` ADD COLUMN `services` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers_land_sell');
		Schema::dropIfExists('offers_land_rent');
	}

}
