<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferLocalMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers_local_sell', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('market'); 
    		$table->tinyInteger('purpose');
    		$table->tinyInteger('building_type'); 
    		$table->tinyInteger('entry'); 
    		$table->string('height');
    		$table->tinyInteger('rooms');
       		$table->tinyInteger('floor'); 
    		$table->tinyInteger('levels');
    		$table->tinyInteger('floors_in_building');  
    		$table->string('year'); 
    		$table->tinyInteger('ownership'); 
    		$table->decimal('rent' , 6 , 2); 
    		$table->string('rent_description'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('windows'); 
    		$table->tinyInteger('heat'); 
    		$table->tinyInteger('available'); 

    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_local_sell` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");


Schema::create('offers_local_rent', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('purpose');
    		$table->tinyInteger('rent_additional')->nullable();
    		$table->tinyInteger('building_type'); 
    		$table->tinyInteger('entry'); 
    		$table->string('height');
    		$table->tinyInteger('rooms');
       		$table->tinyInteger('floor'); 
    		$table->tinyInteger('levels');
    		$table->tinyInteger('floors_in_building');  
    		$table->string('year'); 
    		$table->decimal('rent' , 6 , 2); 
    		$table->integer('quality'); 
    		$table->tinyInteger('windows'); 
    		$table->tinyInteger('heat'); 
    		$table->tinyInteger('available'); 

    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_local_rent` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers_local_sell');
		Schema::dropIfExists('offers_local_rent');
	}

}
