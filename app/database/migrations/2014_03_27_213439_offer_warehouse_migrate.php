<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferWarehouseMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers_warehouse_sell', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('market'); 
    		$table->tinyInteger('purpose');
    		$table->tinyInteger('rooms'); 
    		$table->string('height');
    		$table->tinyInteger('floor'); 
    		$table->tinyInteger('levels');
    		$table->string('year'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('heat'); 
    		

    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_warehouse_sell` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");


		Schema::create('offers_warehouse_rent', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('purpose');
    		$table->tinyInteger('rooms'); 
    		$table->string('height');
    		$table->tinyInteger('floor'); 
    		$table->tinyInteger('levels');
    		$table->string('year'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('heat');

    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_warehouse_rent` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers_warehouse_sell');
		Schema::dropIfExists('offers_warehouse_rent');
	}

}
