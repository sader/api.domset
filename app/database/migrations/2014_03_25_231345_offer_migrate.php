<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		// php artisan migrate --package=cartalyst/sentry

		Schema::create('offers', function($table)
		{
    		$table->increments('id');
    		$table->integer('type_id')->unsigned(); 
    		$table->integer('user_id')->unsigned(); 
    		$table->string('title');
    		$table->string('province'); 
    		$table->string('community'); 
    		$table->string('district');  
    		$table->string('city'); 
    		$table->string('section'); 
    		$table->string('street'); 
    		$table->decimal('surface', 10, 2); 
    		$table->decimal('price', 10, 2); 
    		$table->tinyInteger('price_negotiable'); 
    		$table->decimal('price_surface', 10, 2); 
    		$table->string('lat'); 
    		$table->string('lng'); 
    		$table->text('description'); 
    		$table->string('api_id'); 
    		$table->timestamps();
    		$table->softDeletes();
		});	



	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers');
	}

}


