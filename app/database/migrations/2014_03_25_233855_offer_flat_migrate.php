<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfferFlatMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers_flat_sell', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('market'); 
    		$table->tinyInteger('rooms');
    		$table->tinyInteger('floor'); 
    		$table->tinyInteger('levels'); 
    		$table->tinyInteger('floors_in_building');  
    		$table->tinyInteger('building_type'); 
    		$table->string('year'); 
    		$table->tinyInteger('building_material'); 
    		$table->tinyInteger('ownership'); 
    		$table->decimal('rent'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('installation_quality'); 
    		$table->tinyInteger('windows'); 
    		$table->tinyInteger('kitchen'); 
    		$table->tinyInteger('noise'); 
    		$table->tinyInteger('heat'); 
    		
    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');
		});	

			DB::statement("ALTER TABLE `offers_flat_sell` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");

	
				Schema::create('offers_flat_rent', function($table)
		{
    		$table->integer('id')->unsigned()->primary();
    		$table->tinyInteger('market'); 
    		$table->tinyInteger('rooms');
    		$table->tinyInteger('floor'); 
    		$table->tinyInteger('levels'); 
    		$table->tinyInteger('floors_in_building');  
    		$table->tinyInteger('building_type'); 
    		$table->string('year'); 
    		$table->tinyInteger('building_material'); 
    		$table->tinyInteger('ownership'); 
    		$table->decimal('rent'); 
    		$table->integer('quality'); 
    		$table->tinyInteger('installation_quality'); 
    		$table->tinyInteger('windows'); 
    		$table->tinyInteger('kitchen'); 
    		$table->tinyInteger('noise'); 
    		$table->tinyInteger('heat'); 
    		
    		$table->foreign('id')
      			  ->references('id')->on('offers')
      			  ->onDelete('cascade');

      			  
		});	

		DB::statement("ALTER TABLE `offers_flat_rent` ADD COLUMN `additional` SET ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') NULL DEFAULT NULL");


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('offers_flat_sell');
		Schema::dropIfExists('offers_flat_rent');
	}

}
