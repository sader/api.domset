<?php 

class GroupSeeder extends Seeder
{

public function run()
{
    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
	DB::table('groups')->truncate();
	DB::table('users_groups')->truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');

$group = Sentry::createGroup(array(
        'name'        => 'admin',
        'permissions' => array(
            'admin' => 1,
            'user' => 1,
        ),
    ));

$group = Sentry::createGroup(array(
        'name'        => 'user',
        'permissions' => array(
            'admin' => 0,
            'user' => 1,
        ),
    ));

}



}