<?php
class UserSeeder extends Seeder
{
    
    public function run() {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        
        $user = Sentry::createUser(array('email' => 'sader82@gmail.com', 
                                         'password' => 'admin123', 
                                         'permissions' => array('admin' => 1 , 'user' => 1),
                                         'activated' => true));
        
        $adminGroup = Sentry::findGroupByName('admin');
        $user->addGroup($adminGroup);



        $user = Sentry::createUser(array('email' => 'sader@o2.pl', 
                                         'password' => 'sader123', 
                                         'permissions' => array('user' => 1),
                                         'activated' => true));
        
        $userGroup = Sentry::findGroupByName('user');
        $user->addGroup($userGroup);

    }
}
