<?php
class WallSeeder extends Seeder
{
    
    public function run() {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('walls')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        
        $faker = Faker\Factory::create('en_GB');
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Image($faker));
        
        foreach (range(1, 150) as $i) {
            Wall::create(array(
                'user_id' => rand(1, 2) ,
                'title' => $faker->text(100) ,
                'description' => $faker->text(600) ,
                'image' => $faker->image( public_path() . '/images', 640, 480, 'city', false),
                'created_at' => $faker->dateTimeThisYear()->format('Y-m-d H:i:s') ,
                'updated_at' => $faker->dateTimeThisYear()->format('Y-m-d H:i:s')
            ));
        }
    }
}

