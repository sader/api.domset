<?php


Route::get('logout', array( 'as' => 'logout', 'uses' => 'AuthController@getLogout'));
Route::get('login', array('as' => 'login', 'uses' => 'AuthController@getLogin' ));
Route::post('login', array( 'as' => 'login.post', 'uses' => 'AuthController@postLogin' ));

Route::get('test', function () {
    echo '<html><meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8" /><body><pre>';
  $worker = new ApiWorkCommand(); 
  $worker->fire(); 
});


Route::get('test2', function () {
    echo '<html><meta http-equiv="Content-Type" content="text/xhtml; charset=utf-8" /><body><pre>';
  $worker = new DomsetWorkCommand(); 
  $worker->fire(); 
});

Route::group(array('before' => 'sentry.auth' ) , function () {
    Route::get('/', array('as' => 'wall', 'uses' => 'WallController@show'));
    Route::get('/oferty/{type?}', array( 'as' => 'offer', 'uses' => 'OfferController@index' ));
    Route::get('konfiguracja-api', array( 'as' => 'api_config', 'uses' => 'ApiController@config'));
    Route::post('konfiguracja-api', array( 'as' => 'api_config_save', 'uses' => 'ApiController@saveconfig'));
});


Route::group(array('before' => 'api.auth' , 'prefix' => 'api') , function () {
    
	// add product
	// update product
	// remove product
	// list prodict

    
});
