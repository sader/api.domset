<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DomsetWorkCommand extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'api:domset';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wykonaj zadanie importu  z domsetu';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        
        $user = Sentry::findAllUsersWithAccess(array(
            'admin'
        ));
        
        $this->setOwner($user[0]->id);
        $this->setMainAdapter(new \Offer\Adapter\CoreAdapter());
    }
    
    private $owner;
    private $mainAdapter;
    private $jobs;
    
    public function getMainAdapter() {
        return $this->mainAdapter;
    }
    
    public function setMainAdapter($mainAdapter) {
        $this->mainAdapter = $mainAdapter;
        return $this;
    }
    
    public function getOwner() {
        return $this->owner;
    }
    
    public function setOwner($owner) {
        $this->owner = $owner;
        return $this;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        
        $offerFlatSell = DB::connection('mysql_domset')->table('offer_flat_sell')->get();
        $offerFlatRent = DB::connection('mysql_domset')->table('offer_flat_rent')->get();
        $offerHouseSell = DB::connection('mysql_domset')->table('offer_house_sell')->get();
        $offerHouseRent = DB::connection('mysql_domset')->table('offer_house_rent')->get();
        $offerLocalSell = DB::connection('mysql_domset')->table('offer_local_sell')->get();
        $offerLocalRent = DB::connection('mysql_domset')->table('offer_local_rent')->get();
        $offerWarehouseSell = DB::connection('mysql_domset')->table('offer_warehouse_sell')->get();
        $offerWarehouseRent = DB::connection('mysql_domset')->table('offer_warehouse_rent')->get();
        $offerLandSell = DB::connection('mysql_domset')->table('offer_land_sell')->get();
        $offerLandRent = DB::connection('mysql_domset')->table('offer_land_rent')->get();
        $offers = DB::connection('mysql_domset')->table('offer')->where('status', '=', 1)->get();
        
        $offerBinded = array();
        
        foreach ($offers as $offer) {
            $offerBinded[$offer->id]['data'] = get_object_vars($offer);
            $offerBinded[$offer->id]['images'] = $this->getImagesForOffer($offer->id);
        }
        
        $this->populate($offerFlatSell, $offerBinded, 'FlatSell');
        $this->populate($offerFlatRent, $offerBinded, 'FlatRent');
        $this->populate($offerHouseSell, $offerBinded, 'HouseSell');
        $this->populate($offerHouseRent, $offerBinded, 'HouseRent');
        $this->populate($offerLocalSell, $offerBinded, 'LocalSell');
        $this->populate($offerLocalRent, $offerBinded, 'LocalRent');
        $this->populate($offerLandSell, $offerBinded, 'LandSell');
        $this->populate($offerLandRent, $offerBinded, 'LandRent');
        $this->populate($offerWarehouseSell, $offerBinded, 'WarehouseSell');
        $this->populate($offerWarehouseRent, $offerBinded, 'WarehouseRent');
    }
    
    private function getImagesForOffer($id) {
        $images = DB::connection('mysql_domset')->table('gallery')
                                                ->where('id_offer', '=', $id)
                                                ->where('type', '=', 1)
                                                ->orderBy('ord', 'asc')->get();
        
        $return = array();
        
        foreach ($images as $image) {



            $return[] = 'http://domset.pl/images/offer/'.$id.'/'.$image->name.'.jpg';
        }

        return $return; 
    }
    
    private function populate($data, $offerBinded, $adapter) {
        
        foreach ($data as $ofs) {
            
            $ofs = get_object_vars($ofs);
            
            //  dd($ofs);
            
            if (isset($offerBinded[$ofs['id_offer']])) {
                
                $main = $offerBinded[$ofs['id_offer']]['data'];
                $gallery = $offerBinded[$ofs['id_offer']]['images'];
                
                $this->renameKey($main, 'id', 'api_id');
                $this->renameKey($main, 'id_type', 'type_id');
                $this->renameKey($main, 'id_admin', 'user_id');
                $this->renameKey($main, 'created', 'created_at');
                $this->renameKey($main, 'updated', 'updated_at');
                $this->renameKey($ofs, 'market_visible', 'market');
                
                unset($main['description_pdf']);
                unset($main['status']);
                unset($ofs['available']);
                unset($ofs['id_offer']);
                unset($ofs['additional_payment']);
                
                $main['user_id'] = $this->getOwner();
                
                $offer = array(
                    'adapter' => $adapter,
                    'data' => array(
                        'main' => $main,
                        'detail' => $ofs
                    )
                );
                
                $id = $this->getMainAdapter()->updateOrCreate($offer, $main['api_id']);
                App::make('gallery')->saveGallery($gallery, $id);
                $this->jobs[] = $id;
            }
        }
    }
    
    private function renameKey(&$array, $oldName, $newName) {
        
        if (isset($array[$oldName])) {
            $array[$newName] = $array[$oldName];
            unset($array[$oldName]);
        }
    }
}

