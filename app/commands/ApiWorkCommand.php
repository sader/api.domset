<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ApiWorkCommand extends Command
{
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'api:work';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wykonaj zadanie importu';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        
        
    $aJob = Api::needToDoJob()->oldEnough(3)->oldest()->first();


    if ($aJob) {
        if ($aJob->type == \Crawler\CrawlerAbstract::TYPE_OTODOM) {
           $this->otodomWorker($aJob); 
        } elseif ($aJob->type == Crawler\CrawlerAbstract::TYPE_PARTNER_SITE) {
            $this->partnerSiteWorker($aJob); 
        }

        $aJob->touch();
    }
    }

    private function otodomWorker($aJob)
    {
         
         $crawler = App::make('otodom_crawler');
         $crawler->setProvider($aJob->otodom_url);
         $crawler->setTester('http://otodom.pl/przestronne-3-pokojowe-mieszkanie-id26771487.html');         

         $links = $crawler->getOffersList();

            foreach ($links as $link) {
                
                $finishedJobs = array('user_id' => $aJob->user_id, 'jobs' => array()); 
                $data = $crawler->getOfferDetails($link);
                
                $data['data']['main']['user_id'] = $aJob->user_id;
                $data['data']['main']['api_id'] = $link;
                $data['data']['main']['type_id'] = \Offer\OfferFactory::getIdByName($data['adapter']);

                $db = new \Offer\Adapter\CoreAdapter(); 
                $id =  $db->updateOrCreate($data, $link); 
                
                App::make('gallery')->saveGallery($crawler->getImages() , $id);

                $finishedJobs['jobs'][] = $link; 
            }

            Event::fire('offer.job_finished', $finishedJobs);
    }


    private function galacticaWorker($aJob)
    {
         $crawler = App::make('galactica_crawler');
         $crawler->setProvider($aJob->site_url);
       //  $crawler->setTester('http://otodom.pl/dzialka-rekreacyjna-z-dostepem-do-jeziora-id26504357.html');         

         $links = $crawler->getOffersList();

            foreach ($links as $link) {
                
                $finishedJobs = array('user_id' => $aJob->user_id, 'jobs' => array()); 
                $data = $crawler->getOfferDetails($link);
                
                dd($data); 

                $data['data']['main']['user_id'] = $aJob->user_id;
                $data['data']['main']['api_id'] = $link;
                $data['data']['main']['type_id'] = \Offer\OfferFactory::getIdByName($data['adapter']);

                $db = new \Offer\Adapter\CoreAdapter(); 
                $id =  $db->updateOrCreate($data, $link); 
                
                App::make('gallery')->saveGallery($crawler->getImages() , $id);

                $finishedJobs['jobs'][] = $link; 
            }

            Event::fire('offer.job_finished', $finishedJobs);
    }

    

    private function partnerSiteWorker($aJob)
    {
       switch ($aJob['template_type']) {
           case 1:
                $this->galacticaWorker($aJob); 
               break;
           default:
               # code...
               break;
       }
    }
}

